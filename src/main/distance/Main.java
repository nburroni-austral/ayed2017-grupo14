package main.distance;

/**
 * Created by matiasvenditti on 3/21/17.
 */
public class Main {

    public static void main(String[] args) {

        String str1 = "casa";
        String str2 = "calle";

        System.out.println("Distancia de Hamming entre 'casa' y 'calle': " + Distance.hamming(str1,str2));
        System.out.println("Distancia de Levenshtein entre 'casa' y 'calle': " + Distance.levenshtein(str1,str2));

    }
}
