package main.distance;

/**
 * This class contains the Hamming and Levenshtein algorithms for word distances.
 */
public class Distance {


    /**
     * Private method that returns an integer which represents the number of different characters between two
     * Strings. If one of the String's lenght is greater than the other, the difference between
     * lengths will be part of the result.
     * hamming method is implemented asuming str2 is shorter than str1.
     * @param str1 a String passed as a parameter to be compared with str2.
     * @param str2 a String passed as a parameter to be compared with str1.
     * @return the amount of different characters between two Strings.
     */
    private static int hammingAlgorithm(String str1, String str2){
        int result = 0;
        int i = 0;
        while(i < str2.length()){
            if (str1.charAt(i) != str2.charAt(i))
                result++;
            i++;
        }
        return result + (str1.length() - str2.length());
    }

    /**
     * Method that is used in combination with the hamming method.
     * As hamming is based on the fact that str2 is greater than str1, bigger just
     * calls hamming if str2 is indeed greater than str1, and calls hamming with
     * its parameters switched if that is not the case.
     * @param str1 String passed as a parameter.
     * @param str2 String passed as a parameter.
     * @return the amount of different characters between str1 and str2.
     */
    public static int hamming(String str1, String str2){
        return (str1.length() >= str2.length())? hammingAlgorithm(str1, str2) : hamming(str2, str1);
    }

    /**
     * @param a integer to be compared.
     * @param b integer to be compared.
     * @param c integer to be compared.
     * @return minimum integer between the three integers passed as arguments.
     */
    private static int minimum(int a, int b, int c) {
        if(a<=b && a<=c){
            return a;
        }
        if(b<=a && b<=c){
            return b;
        }
        return c;
    }

    /**
     * Receives two strings and converts them to char arrays, then calls the computeLevenshteinDistance
     * passing both char arrays as arguments.
     * @param str1 first word to be compared.
     * @param str2 second word to be compared.
     * @return
     */
    public static int levenshtein(String str1, String str2) {
        return computeLevenshteinDistance(str1.toCharArray(),
                str2.toCharArray());
    }

    /**
     * This method calls the setIndex method, so after creating a matrix, compares the matrix elements
     * in the positions [i-1][j], [i][j-1], and [i-1][j-1] with the use of the method minimum. Finally,
     * if the chars at the index positions are equal, 1 is added to the minimum of the three values, resulting
     * in the amount of changes getting the value of the matrix at [length of first word][first of second word].
     * @param str1 char array representing the first word.
     * @param str2 char array representing the second word.
     * @return integer number representing the amount of changes. The changes are insertions, swaps, and deletions.
     */
    private static int computeLevenshteinDistance(char [] str1, char [] str2) {
        int[][] distance = setIndex(str1, str2);

        for(int i=1;i<=str1.length;i++){
            for(int j=1;j<=str2.length;j++){
                distance[i][j]= minimum(distance[i-1][j]+1,
                        distance[i][j-1]+1,
                        distance[i-1][j-1]+
                                ((str1[i-1]==str2[j-1])?0:1));
            }
        }
        return distance[str1.length][str2.length];

    }

    /**
     * Returns a matrix of (n+1) x (m+1), n being the length of the first char array, and m the length of the second one.
     * The matrix is filled with the corresponding index in the first column and first row.
     * @param str1 Char array which references the first word
     * @param str2 Char array which references the second word
     * @return double array of integers.
     */
    private static int[][] setIndex(char[] str1, char[] str2){
        int [][]distance = new int[str1.length+1][str2.length+1];

        for(int i=0;i<=str1.length;i++)
            distance[i][0]=i;
        for(int j=0;j<=str2.length;j++)
            distance[0][j]=j;
        return distance;
    }
}


