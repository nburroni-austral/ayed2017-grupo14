package main.tp3.ej2;

import java.io.File;

/**
 * Created by matiasvenditti on 3/28/17.
 */
public class TestLexicographic {

    public static void main(String[] args) {
        LexicographicAnalizer analizer = new LexicographicAnalizer();

        File textFile = new File("/Users/matiasvenditti/IdeaProjects/Ayed/ayed2017-grupo14/src/main/tp3/ej2/TestText.rtf");
        System.out.println(analizer.analyze(textFile));
    }
}
