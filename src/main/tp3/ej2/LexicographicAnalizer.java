package main.tp3.ej2;

import main.stack.DynamicStack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * LexicographicAnalizers to know wether a text file has same amount of opening keys,
 * parenthesis, or square brackets, or not.
 */
public class LexicographicAnalizer {

    private DynamicStack<Character>charStack;

    /**
     * Constructor method to create LexicographicAnalizer
     */
    public LexicographicAnalizer(){
        charStack = new DynamicStack<>();
    }

    /**
     * Converts a text file to String.
     * @param file passed as argument to be converted into String.
     * @return String value equivalent to the text file.
     * @throws IOException if the file doesn't exist or is not found in the directory.
     */
    private String readFile(File file) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        try{
            while((line = reader.readLine()) != null){
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            return stringBuilder.toString();
        }
        finally {
            reader.close();
        }
    }

    /**
     * Checks if the text file passed as argument is balanced, being balanced when each
     * {,[,( have a closing character somewhere in the code.
     * If there is an initial closing character an exception is thrown, same if the closing character
     * doesn't match the top element in the stack of opening characters.
     * @param file in question to analize.
     */

    private void privateAnalyze(File file){
        try{
            String fileString = readFile(file);
            for (int i = 0; i < fileString.length(); i++) {
                if (isOpened(fileString.charAt(i))){
                    charStack.push(fileString.charAt(i));
                }
                else if (isClosed(fileString.charAt(i))){
                    if (charStack.isEmpty()){
                        throw new RuntimeException("Closing character with no previous opnening character in the code.");
                    }
                    else{
                        if(match(charStack.peek(),fileString.charAt(i))){
                            charStack.pop();
                        }
                        else{
                            throw new RuntimeException("Character " + fileString.charAt(i) + " doesn't match with last opening character.");
                        }
                    }
                }
            }
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * If the text is unbalanced returns a String indicating by how many characters is unbalanced.
     * @param file to analyze.
     * @return String indicating if the text is balanced or not.
     */
    public String analyze(File file){
        String result;
        privateAnalyze(file);
        if (!charStack.isEmpty()){
            result = "Text is unbalanced by " + charStack.size() + " character/s.";
        }
        else{
            result = "Text is correctly balanced!";
        }
        return result;
    }

    /**
     * Indicates if the characters passed as arguments both belong to the same group, and the first
     * character is an opening character, and finally the second being a closing one.
     * @param c1, first character
     * @param c2, second character
     * @return true if the first and second characters belong to the same group.
     */
    private static boolean match(char c1, char c2){
        return ((c1 == '{' && c2 == '}') || (c1 == '[' && c2 == ']') || (c1 == '(' && c2 == ')'));
    }

    /**
     * Indicates if the character is an opening character.
     * @param character
     * @return true if it's an opening key, parenthesis, or square bracket, false if not.
     */
    private static boolean isOpened(char character){
        return (character == '{' || character == '[' || character == '(');
    }

    /**
     * Indicates if the character is a closing character.
     * @param character
     * @return false if it's a closing key, parenthesis, or square bracket, false if not.
     */
    private static boolean isClosed(char character){
        return (character == '}' || character == ']' || character == ')');
    }
}
