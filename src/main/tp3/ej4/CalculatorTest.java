package main.tp3.ej4;

/**
 * Created by Brian Froschauer on 29/3/2017.
 */
public class CalculatorTest {
    public CalculatorTest() {
    }

    public static void main(String[] agrs) {
        Calculator c = new Calculator();
        System.out.println(c.evaluate("2+4-8/2*4-1*5*5/9/8/6-1=")); // -11.05787037037037
    }
}
