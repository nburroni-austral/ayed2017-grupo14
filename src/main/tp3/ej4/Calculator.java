package main.tp3.ej4;

import java.util.ArrayList;
import main.stack.DynamicStack;

/**
 * Created by Brian Froschauer on 29/3/2017.
 */

public class Calculator {
    private DynamicStack<Character> operations = new DynamicStack<>();
    private DynamicStack<Character> output = new DynamicStack<>();
    private ArrayList<Character> validOperators = new ArrayList<>();

    /**
     * Creates a calculator with basic operations.
     */
    public Calculator() {
        this.validOperators.add('+');
        this.validOperators.add('-');
        this.validOperators.add('*');
        this.validOperators.add('/');
    }

    /**
     * Method that evaluates mathematical expression.
     * @param expression in post-fix format.
     * @return double representing the evaluated expression.
     */
    public double evaluate(String expression) {
        String postfix = convert(expression);
        DynamicStack<Double> stack = new DynamicStack<>();

        for(int i = 0; i < postfix.length(); i++) {
            if(isANumber(postfix.charAt(i))) {
                stack.push(Double.valueOf(String.valueOf(postfix.charAt(i))));
            }

            if(this.isAnOperator(postfix.charAt(i))) {
                double a = stack.peek();
                stack.pop();
                double b = stack.peek();
                stack.pop();

                if(validOperators.get(0) == postfix.charAt(i)) {
                    stack.push(b + a);
                }
                if(validOperators.get(1) == postfix.charAt(i)) {
                    stack.push(b - a);
                }
                if(validOperators.get(2) == postfix.charAt(i)) {
                    stack.push(b * a);
                }
                if(validOperators.get(3) == postfix.charAt(i)) {
                    stack.push(b / a);
                }
            }
        }

        return stack.peek();
    }

    /**
     * Converts in-fix to post-fix.
     * @param infix of type String, with in-fix format converting it to post-fix.
     * @return String expression in post-fix format.
     */
    private String convert(String infix) {
        String postfix = "";
        DynamicStack<Character> stackAux = new DynamicStack<>();

        for(int i = 0; i < infix.length(); i++) {
            if(infix.charAt(i) == '=') {
                emptyOperations();
            }

            if(this.isAnOperator(infix.charAt(i))) {
                if(operations.isEmpty()) {
                    operations.push(infix.charAt(i));
                } else {
                    if(infix.charAt(i) == '+' || infix.charAt(i) == '-') {
                        emptyOperations();
                        operations.push(infix.charAt(i));
                } else {
                        while(!operations.isEmpty()) {
                            if(operations.peek() == '*' || operations.peek() == '/') {
                                output.push(operations.peek());
                                operations.pop();
                            } else {
                                stackAux.push(operations.peek());
                                operations.pop();
                            }
                        }
                        while(!stackAux.isEmpty()) {
                            operations.push(stackAux.peek());
                            stackAux.pop();
                        }
                        operations.push(infix.charAt(i));
                    }
                }
            } else {
                output.push(infix.charAt(i));
            }
        }

        while(!output.isEmpty()) {
            postfix += output.peek();
            output.pop();
        }

        String postfixAux = "";

        for(int i = postfix.length()-1; i >= 0; i--) {
            postfixAux += postfix.charAt(i);
        }
        return postfixAux;
    }

    /**
     * Indicates wether the passed argument is an operation.
     * @param c character passed as an argument.
     * @return true if character passed as argument is a valid operation, false if not.
     */
    private boolean isAnOperator(char c) {
        return validOperators.contains(c);
    }

    /**
     * Indicates if the passed argument is a number.
     * @param c character passed as an argument.
     * @return true if the character is a number, false if not.
     */
    private boolean isANumber(char c) {
        try {
            Double.parseDouble(String.valueOf(c));
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Method that passes every operation from the operation stack to the output stack.
     */
    private void emptyOperations() {
        while(!operations.isEmpty()) {
            output.push(this.operations.peek());
            operations.pop();
        }
    }
}