package main.tp3.ej3;

/**
 * Created by matiasvenditti on 3/23/17.
 */
public class Car {

    private String plate;
    private String brand;
    private String model;
    private String color;

    public Car(String brand, String model, String plate, String color){
        this.brand = brand;
        this.model = model;
        this.plate = plate;
        this.color = color;
    }

    public String getPlate() { return plate; }

    public String getBrand() { return brand; }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }


    public boolean equals(Object o){
        if (this == o){
            return true;
        }
        if (o.getClass() != this.getClass()){
            return false;
        }
        Car car = (Car) o;
        return (car.getBrand().equals(getBrand()) && car.getModel().equals(getModel()) && car.getPlate().equals(getPlate()) && car.getColor().equals(getColor()))? true : false;
    }
}
