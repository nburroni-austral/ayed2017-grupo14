package main.tp3.ej3;
import main.stack.DynamicStack;



/**
 * Created by matiasvenditti on 3/23/17.
 * A parking lot as a way of containing cars by adding them, removing them, and getting profit
 * upon adding them to the parking lot.
 */
public class ParkingLot {

    private final int CAPACITY = 50;
    private int carAmount = 0;
    private DynamicStack<Car> carStack;
    private DynamicStack<Car> aux;
    private final double RATE = 5;
    private int profits = 0;

    /**
     * Creates a parking lot with a stack of cars, a capacity, a rate, and counters like
     * carAmount, profits, and an auxiliary stack "aux".
     */
    public ParkingLot(){
        carStack = new DynamicStack<>();
        aux = new DynamicStack<>();
    }

    /**
     * If the amount of cars in the parking lot doesn't excede the maximum capacity,
     * the car is added to the stack and is charged $5.
     * @param car to be added to the stack of cars representing the parking lot.
     */
    public void addCar(Car car){
        if (carAmount < CAPACITY){
            carStack.push(car);
            profits += RATE;
            carAmount++;
        }
        else{
            throw new RuntimeException("Parking lot capacity surpassed.");
        }
    }

    /**
     *If the car is found in the stack, the car stack is emptied, stacking the values
     *in an auxiliary stack, and when the value of interest is found on the car stack,
     *its simply removed. Then the car stack is refilled with the values passed in the
     *auxiliary stack. Else, an exception is thrown.
     * @param car to be removed from the parking lot.
     */
    public void removeCar(Car car){
        int index = carStack.size();
        int counter = 0;

        for (int i = 0; i <= index; i++) {
            if (index > counter && carStack.peek().equals(car)){
                carStack.pop();
                break;
            }
            else if (index > counter && !carStack.peek().equals(car)){
                aux.push(carStack.peek());
                carStack.pop();
            }
            else{
                throw new RuntimeException("Car not found in the parking lot.");
            }
            counter++;
        }


        while(!aux.isEmpty()){
            carStack.push(aux.peek());
            aux.pop();
        }
    }

    /**
     *
     * @return integer number representing the profits of the parking lot.
     */
    public int getProfits(){
        return profits;
    }

}
