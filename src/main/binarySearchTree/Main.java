package main.binarySearchTree;

/**
 * Created by matiasvenditti on 4/15/17.
 */
public class Main {

    public static void main(String[] args) {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();

        System.out.println("Binary search tree is empty: " + bst.isEmpty());
        bst.insert(4);
        bst.insert(3);
        bst.insert(2);
        bst.insert(1);
        bst.insert(6);

        System.out.println("Inserting values 4,3,2,1,6 to binary search tree.");

        System.out.println("Binary search tree is empty: " + bst.isEmpty());

        System.out.println("Binary search tree contains value 4: " + bst.exists(4));
        System.out.println("Binary search tree contains value 3: " + bst.exists(3));
        System.out.println("Binary search tree contains value 2: " + bst.exists(2));
        System.out.println("Binary search tree contains value 1: " + bst.exists(1));
        System.out.println("Binary search tree contains value 0: " + bst.exists(0));

        System.out.println("Left child: " + bst.leftChild().getRoot()); //should be equal to 3.
        System.out.println("Right child: " + bst.rightChild().getRoot()); //should be equal to 6.

        System.out.println("Root element: " + bst.getRoot()); //should be equal to 4.
        System.out.println("Minimum element: " + bst.getMin()); //should be equal to 1.
        System.out.println("Maximum element: " + bst.getMax()); //should be equal to 6.

        System.out.println("Elements: " + bst.display().toString());

        System.out.println("Deleting every value.");

        System.out.println("Binary search tree is empty: " + bst.isEmpty());

        System.out.println("Binary search tree contains value 4: " + bst.exists(4));
        System.out.println("Binary search tree contains value 3: " + bst.exists(3));
        System.out.println("Binary search tree contains value 2: " + bst.exists(2));
        System.out.println("Binary search tree contains value 1: " + bst.exists(1));
        System.out.println("Binary search tree contains value 0: " + bst.exists(0));

        System.out.println("Left child: " + bst.leftChild().getRoot()); //should be equal to 3.
        System.out.println("Right child: " + bst.rightChild().getRoot()); //should be equal to 6.

        System.out.println("Root element: " + bst.getRoot()); //should be equal to 4.
        System.out.println("Minimum element: " + bst.getMin()); //should be equal to 1.
        System.out.println("Maximum element: " + bst.getMax()); //should be equal to 6.


        System.out.println("Elements: " + bst.display().toString());




    }
}
