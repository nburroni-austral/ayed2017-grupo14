package main.binarySearchTree;

import main.binaryTree.DoubleNode;
import java.util.ArrayList;

/**
 * Created by matiasvenditti on 4/13/17.
 */
public class BinarySearchTree<T> {

    private DoubleNode<T> root;
    private ArrayList<T> display;

    public BinarySearchTree(){
        root = null;
        display = new ArrayList<>();
    }

    public boolean isEmpty(){
        return root == null;
    }

    public T getRoot(){
        return root.element;
    }

    public DoubleNode<T> getRootNode(){
        return root;
    }


    public BinarySearchTree leftChild(){
        if (root == null){
            throw new RuntimeException("No left child.");
        }
        else{
            BinarySearchTree<T> result = new BinarySearchTree<>();
            result.root = root.left;
            return result;
        }
    }

    public BinarySearchTree rightChild(){
        if (root == null){
            throw new RuntimeException("No right child.");
        }
        else{
            BinarySearchTree<T> result = new BinarySearchTree<>();
            result.root = root.right;
            return result;
        }
    }

    public boolean exists(Comparable<T> x){
        return exists(root, x);
    }


    public T getMin(){
        if (!isEmpty()){
            return getMin(root).element;
        }
        else{
            throw new RuntimeException("No minimum value.");
        }
    }

    public T getMax(){
        if (!isEmpty()){
            return getMax(root).element;
        }
        else{
            throw new RuntimeException("No maximum value.");
        }
    }

    public T search(Comparable<T> x){
        return search(root, x).element;
    }

    public void insert(Comparable<T> x){
        root = insert(root, x);
    }

    public void delete(Comparable<T> x){
        if (exists(root, x)){
            root = delete(root, x);
        }
        else{
            throw new RuntimeException("Cannot remove element.");
        }
    }

    private boolean exists(DoubleNode<T> node, Comparable<T> x){
        if (node == null){
            return false;
        }
        if (x.compareTo(node.element) == 0){
            return true;
        }
        else if (x.compareTo(node.element) < 0){
            return exists(node.left,x);
        }
        else {
            return exists(node.right, x);
        }
    }

    private DoubleNode<T> getMin(DoubleNode<T> node){
        if (node.left == null){
            return node;
        }
        else{
            return getMin(node.left);
        }
    }


    private DoubleNode<T> getMax(DoubleNode<T> node){
        if (node.right == null){
            return node;
        }
        else{
            return getMax(node.right);
        }
    }

    private DoubleNode<T> search(DoubleNode<T> node, Comparable<T> x){
        if (x.compareTo(node.element) == 0){
            return node;
        }
        else if (x.compareTo(node.element) < 0){
            return search(node.left, x);
        }
        else {
            return search(node.right, x);
        }
    }


    private DoubleNode<T> insert(DoubleNode<T> node, Comparable<T> x){
        if (node == null){
            node = new DoubleNode<>((T) x);
        }
        else if (x.compareTo(node.element) < 0){
            node.left = insert(node.left, x);
        }
        else{
            node.right = insert(node.right, x);
        }
        return node;
    }

    private DoubleNode<T> delete(DoubleNode<T> node, Comparable<T> x){
        if (x.compareTo(node.element) < 0){
            node.left = delete(node.left, x);
        }
        else if (x.compareTo(node.element) > 0){
            node.right = delete(node.right, x);
        }
        else{
            if (node.left != null && node.right != null){
                node.element = getMin(node.right).element;
                node.right = deleteMin(node.right);
            }
            else if(node.left != null){
                node = node.left;
            }
            else{
                node = node.right;
            }
        }
        return node;
    }

    private DoubleNode<T> deleteMin(DoubleNode<T> node){
        if (node.left != null){
            node.left = deleteMin(node.left);
        }
        else{
            node = node.right;
        }
        return node;
    }

    //AUXILIARY.

    public ArrayList<T> display(){
        display = new ArrayList<>();
        display(root);
        return display;
    }

    private void display(DoubleNode<T> node){
        if (node != null){
            display(node.left);
            display.add(node.element);
            display(node.right);
        }
    }

}
