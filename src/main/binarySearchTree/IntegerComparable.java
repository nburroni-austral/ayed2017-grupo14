package main.binarySearchTree;

/**
 * Created by matiasvenditti on 4/17/17.
 */
public class IntegerComparable implements Comparable<Integer> {

    private int value;

    public IntegerComparable(int value) {
        this.value = value;
    }

    @Override
    public int compareTo(Integer o) {

        return this.value - o;
    }
}
