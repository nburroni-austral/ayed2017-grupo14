package main.soccer;

/**
 * Created by Brian Froschauer on 17/4/2017.
 */
public class Team {
    private String name;
    private int points, auxPoint;
    private boolean win, tie, lose;
    private String result;

    public Team(String name, int points) {
        this.name = name;
        this.points = points;
        this.auxPoint = 0;
        result = "";
        win = false;
        tie = false;
        lose = false;
    }

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public void setAuxPoint(int point) {
        auxPoint = point;
    }

    public int getAuxPoint() {
        return auxPoint;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setWin(boolean b) {
        win = b;
    }

    public boolean getWin() {
        return win;
    }

    public void setTie(boolean b) {
        tie = b;
    }

    public boolean getTie() {
        return tie;
    }

    public void setLose(boolean b) {
        lose = b;
    }

    public boolean getLose() {
        return lose;
    }

    public void resetValues() {
        setWin(false);
        setTie(false);
        setLose(false);
        result = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        if (points != team.points) return false;
        return name.equals(team.name);

    }
}
