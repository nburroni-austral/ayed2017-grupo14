package main.soccer;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Brian Froschauer on 17/4/2017.
 */
public class SoccerTable {

    private int m;
    private int n;
    private ArrayList<Team> teams;
    private ArrayList<Match> matches;

    public SoccerTable() {
        teams = new ArrayList<>();
        matches = new ArrayList<>();
        addCase();
    }

    public void addCase() {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        addNumbers(m, n);

        for (int i = 0; i < m; i++) {
            String name = sc.next();
            int points = sc.nextInt();
            addTeam(name, points);
        }

        for (int i = 0; i < n; i++) {
            String local = sc.next();
            String visitant = sc.next();
            addMatch(local, visitant);
        }

        String aux = sc.next();
        if (aux.equals("-1")) backtracking();
        if (aux.equals(" ")) addCase();
    }

    public void addNumbers(int m, int n) {
        checkValues(m, n);
        this.m = m;
        this.n = n;
    }

    public void addTeam(String name, int points) {
        Team team = new Team(name, points);
        if (teams.contains(team)) throw new RuntimeException("Team already exist.");
        teams.add(team);
    }

    public void addMatch(String local, String visitant) {
        if (search(local) && search(visitant)) {
            Match match = new Match(get(local), get(visitant));
            matches.add(match);
        }
    }

    private boolean search(String name) {
        for (Team team : teams) {
            if (team.getName().equals(name)) return true;
        }
        return false;
    }

    private Team get(String name) {
        for (Team team : teams) {
            if (team.getName().equals(name)) return team;
        }
        throw new RuntimeException("Team not found.");
    }

    public void backtracking() {
        int i = 0;
            while (i < matches.size()) {
            if (matches.get(i).canWin()){
                matches.get(i).getLocal().setAuxPoint(matches.get(i).getLocal().getAuxPoint()+3);
                i++;
            } else {
                if (matches.get(i).canTie()) {
                    matches.get(i).getLocal().setAuxPoint(matches.get(i).getLocal().getAuxPoint()+1);
                    matches.get(i).getVisitant().setAuxPoint(matches.get(i).getVisitant().getAuxPoint()+1);
                    i++;
                } else {
                    if (matches.get(i).canLose()) {
                        matches.get(i).getVisitant().setAuxPoint(matches.get(i).getVisitant().getAuxPoint()+3);
                        i++;
                    } else {
                        if (checkResult()) {
                            i++;
                        } else {
                            matches.get(i).getLocal().resetValues();
                            i--;
                            matches.get(i).previous();
                        }
                    }
                }
            }
        }
        printEntry();
    }

    public boolean checkResult() {
        for (Team team : teams) {
            if (team.getAuxPoint() != team.getPoints()) {
                return false;
            }
        }
        return true;
    }


    public void checkValues(int m, int n) {
        if (m < 0 || n < 0 || m > 10 || n > 20) {
            throw new RuntimeException("Invalid m or n");
        }
    }

    public void printEntry() {
        System.out.println(m + " " + n);

        for (Team team : teams) {
            System.out.println(team.getName() + " " + team.getPoints());
        }

        for (Match match : matches) {
            System.out.println(match.getLocal().getName() + " " + match.getVisitant().getName());
        }

        String result = "";

        for (Team team : teams) {
            result += team.getResult();
        }
        System.out.println(result);
    }
}
