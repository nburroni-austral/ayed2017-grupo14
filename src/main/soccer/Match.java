package main.soccer;

/**
 * Created by Brian Froschauer on 17/4/2017.
 */
public class Match {
    private Team local;
    private Team visitant;

    public Match(Team local, Team visitant) {
        this.local = local;
        this.visitant = visitant;
    }

    public Team getLocal() {
        return local;
    }

    public Team getVisitant() {
        return visitant;
    }

    public boolean canWin() {
        if (local.getPoints() >= 3 && local.getAuxPoint()+3 <= local.getPoints() && !local.getWin()) {
            local.setWin(true);
            local.setResult("1");
            return true;
        }
        return false;
    }

    public boolean canTie() {
        if (local.getPoints() >= 1 && visitant.getPoints() >= 1 &&
            local.getAuxPoint()+1 <= local.getPoints() &&
            visitant.getAuxPoint()+1 <= visitant.getPoints() && !local.getTie()) {
            local.setTie(true);
            local.setResult("X");
            return true;
        }
        return false;
    }

    public boolean canLose() {
        if (visitant.getPoints() >= 3 && visitant.getAuxPoint()+3 <= visitant.getPoints() && !local.getLose()) {
            local.setLose(true);
            local.setResult("2");
            return true;
        }
        return false;
    }

    public void previous() {
        if (local.getWin()) {
            local.setAuxPoint(local.getAuxPoint()-3);
            local.setResult("");
        }

        if (local.getTie()) {
            local.setAuxPoint(local.getAuxPoint()-1);
            visitant.setAuxPoint(visitant.getAuxPoint()-1);
            local.setResult("");
        }

        if (local.getLose()) {
            visitant.setAuxPoint(visitant.getAuxPoint()-3);
            local.setResult("");
        }
    }
}
