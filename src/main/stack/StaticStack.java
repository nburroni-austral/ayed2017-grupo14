package main.stack;

import struct.istruct.Stack;

/**
 * Created by Brian Froschauer on 22/3/2017.
 */

public class StaticStack<T> implements Stack<T> {

    private int top;
    private int capacity;
    private Object[] data;

    public StaticStack(int capacity) {
        top = -1;
        this.capacity = capacity;
        data = new Object[capacity];
    }

    @Override
    public void push(T o) {
        if(top+1 == data.length) grow();
        top++;
        data[top] = o;
    }

    @Override
    public void pop() {
        top--;
    }

    @Override
    public T peek() {
        if (!isEmpty()) {
            return (T) data[top];
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return top == -1;
    }

    @Override
    public int size() {
        return top +1;
    }

    @Override
    public void empty() {
        top = -1;
    }

    private void grow() {
        Object[] dataAux = new Object[capacity*2];
        for (int i = 0; i < capacity; i++) {
            dataAux[i] = data[i];
        }
    }

    public int getCapacity(){
        return capacity;
    }
}
