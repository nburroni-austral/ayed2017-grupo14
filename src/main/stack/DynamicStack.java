package main.stack;

import struct.istruct.Stack;

/**
 * Created by Brian Froschauer on 22/3/2017.
 */

public class DynamicStack<T> implements Stack<T> {
    private Node top;
    private int size;

    public DynamicStack() {
        top = null;
        size = 0;
    }

    @Override
    public void push(T o) {
        Node current = new Node();
        current.data = o;
        current.next = top;
        top = current;
        size++;
    }

    @Override
    public void pop() {
        top = top.next;
        size--;
    }

    @Override
    public T peek() {
        if (!isEmpty()) {
            return (T) top.data;
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        while (top != null) {
            pop();
        }
    }

    private class Node{

        T data;
        Node next;


    }
}
