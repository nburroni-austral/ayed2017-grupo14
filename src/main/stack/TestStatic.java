package main.stack;


/**
 * Created by Brian Froschauer on 22/3/2017.
 */
public class TestStatic {
    public static void main(String[] args) {
        StaticStack s = new StaticStack(10);
        System.out.println(s.isEmpty());
        System.out.println(s.size());
        s.push("Elemento 1");
        s.push("Elemento 2");
        System.out.println(s.peek());
        System.out.println(s.isEmpty());
        System.out.println(s.size());
        s.pop();
        System.out.println(s.size());
        System.out.println(s.peek());
        s.push("Elemento 3");
        s.push("Elemento 4");
        s.empty();
        System.out.println(s.size());
        System.out.println(s.isEmpty());
    }
}
