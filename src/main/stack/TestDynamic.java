package main.stack;

/**
 * Created by Brian Froschauer on 23/3/2017.
 */
public class TestDynamic {
    public static void main(String[] args) {
        DynamicStack<String> d = new DynamicStack<>();
        System.out.println(d.isEmpty());
        System.out.println(d.size());
        d.push("Elemento 1");
        d.push("Elemento 2");
        System.out.println(d.peek());
        System.out.println(d.isEmpty());
        System.out.println(d.size());
        d.pop();
        System.out.println(d.size());
        System.out.println(d.peek());
        d.push("Elemento 3");
        d.push("Elemento 4");
        d.empty();
        System.out.println(d.size());
        System.out.println(d.isEmpty());
    }
}
