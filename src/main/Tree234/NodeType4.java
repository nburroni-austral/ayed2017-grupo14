package main.Tree234;

/**
 * Created by micaeladominguez on 07/06/17.
 */
public class NodeType4 extends Node{
    private Integer k1, k2, k3;
    private Node p1, p2, p3, p4;

    public NodeType4(Integer k1, Integer k2, Integer k3, Node p1, Node p2, Node p3, Node p4) {
        this.k1 = k1;
        this.k2 = k2;
        this.k3 = k3;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        type = 4;
    }

    public NodeType4(Integer k1, Integer k2, Integer k3) {
        this.k1 = k1;
        this.k2 = k2;
        this.k3 = k3;
    }

    public NodeType4() {}

    public Integer getK1() {
        return k1;
    }

    public void setK1(Integer k1) {
        this.k1 = k1;
    }

    public Integer getK2() {
        return k2;
    }

    public void setK2(Integer k2) {
        this.k2 = k2;
    }

    public Integer getK3() {
        return k3;
    }

    public void setK3(Integer k3) {
        this.k3 = k3;
    }

    public Node getP1() {
        return p1;
    }

    public void setP1(Node p1) {
        this.p1 = p1;
    }

    public Node getP2() {
        return p2;
    }

    public void setP2(Node p2) {
        this.p2 = p2;
    }

    public Node getP3() {
        return p3;
    }

    public void setP3(Node p3) {
        this.p3 = p3;
    }

    public Node getP4() {
        return p4;
    }

    public void setP4(Node p4) {
        this.p4 = p4;
    }
}
