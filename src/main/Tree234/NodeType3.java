package main.Tree234;

/**
 * Created by micaeladominguez on 07/06/17.
 */
public class NodeType3 extends Node{
    private Integer k1, k2;
    private Node p1, p2, p3;

    public NodeType3(Integer k1, Integer k2, Node p1, Node p2, Node p3) {
        this.k1 = k1;
        this.k2 = k2;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        type = 3;
    }

    private void convert(Integer k) {
        NodeType4 node4 = new NodeType4();
        node4.setFather(getFather());
        node4.setP1(p1);
        node4.setP3(p2);
        node4.setP4(p3);

        if (k.compareTo(k1) < 0) {
            node4.setK3(k2);
            node4.setK2(k1);
            node4.setK1(k);
        }

        if (k.compareTo(k2) > 0) {
            node4.setK1(k1);
            node4.setK2(k2);
            node4.setK3(k);
        }

        if (k.compareTo(k1) > 0 && k.compareTo(k2) < 0) {
            node4.setK1(k1);
            node4.setK2(k);
            node4.setK3(k2);
        }

    }

    public void insert(Integer k) {
        if (k1 == null && k2 == null) k1 = k;

        if (k1 == null && k2 != null) {
            if (k2.compareTo(k) < 0) {
                k1 = k2;
                k2 = k;
            } else {
                k1 = k;
            }
        }

        if (k1 != null && k2 == null) {
            if (k1.compareTo(k) < 0) {
                k2 = k1;
                k1 = k;
            } else {
                k2 = k;
            }
        }

        if (k1 != null && k2 != null) {
            convert(k);
        }
    }

    public NodeType3(Integer k1, Integer k2) {
        this.k1 = k1;
        this.k2 = k2;
    }

    public NodeType3() {}

    public Integer getK1() {
        return k1;
    }

    public void setK1(Integer k1) {
        this.k1 = k1;
    }

    public Integer getK2() {
        return k2;
    }

    public void setK2(Integer k2) {
        this.k2 = k2;
    }

    public Node getP1() {
        return p1;
    }

    public void setP1(Node p1) {
        this.p1 = p1;
    }

    public Node getP2() {
        return p2;
    }

    public void setP2(Node p2) {
        this.p2 = p2;
    }

    public Node getP3() {
        return p3;
    }

    public void setP3(Node p3) {
        this.p3 = p3;
    }
}
