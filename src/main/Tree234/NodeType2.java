package main.Tree234;

/**
 * Created by micaeladominguez on 07/06/17.
 */
public class NodeType2 extends Node {
    private Integer k;
    private Node p1, p2;

    public NodeType2(Integer k, Node p1, Node p2) {
        this.k = k;
        this.p1 = p1;
        this.p2 = p2;
        type = 2;
    }

    public NodeType2(Integer k) {
        this.k = k;
    }

    public NodeType2() {}

    private void convert(Integer k) {
        NodeType3 node3 = new NodeType3();
        node3.setFather(getFather());
        node3.setP1(p1);
        node3.setP3(p2);

        if (this.k.compareTo(k) < 0) {
            node3.setK1(this.k);
            node3.setK2(k);
        } else {
            node3.setK1(k);
            node3.setK2(this.k);
        }
    }

    public void insert(Integer k) {
        if (this.k == null) this.k = k;
        else convert(k);
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public Node getP1() {
        return p1;
    }

    public void setP1(Node p1) {
        this.p1 = p1;
    }

    public Node getP2() {
        return p2;
    }

    public void setP2(Node p2) {
        this.p2 = p2;
    }
}
