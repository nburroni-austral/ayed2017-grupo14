package main.Tree234;

/**
 * Created by micaeladominguez on 07/06/17.
 */
public class Node {
    private Node father;
    protected int type;

    public Node getFather() {
        return father;
    }

    public void setFather(Node father) {
        this.father = father;
    }

}
