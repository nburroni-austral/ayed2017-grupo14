package main.horse.view;

import main.horse.controller.Game;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brian Froschauer on 3/4/2017.
 */
public class BoardFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final int CELL_SIZE = 65;

    private BoardPanel boardPanel;
    private Game game;
    private List<StackPanel> stackPanels;
    private JButton next = new JButton("Next");

    public BoardFrame(Game game) throws IOException {
        this.game = game;

        setLayout(null);
        setSize(game.getBoard().getDimension() * CELL_SIZE + 80 +
                game.getMovements() * StackPanel.PANEL_WIDTH, game.getBoard().getDimension() * CELL_SIZE +100);

        initBoard(game);
        initButton(game);
        initStacks(game);
    }

    private void initBoard(Game game) throws IOException {
        boardPanel = new BoardPanel(game.getBoard().getDimension(), game.getBoard().getDimension(), CELL_SIZE, game);
        add(boardPanel);
    }

    private void initButton(final Game game) {
        next.setLocation(game.getBoard().getDimension() * CELL_SIZE, (game.getBoard().getDimension() * CELL_SIZE)/2);
        next.setSize(80, 30);
        add(next);
        next.addActionListener(e -> game.next());
    }

    private void initStacks(Game game){
        stackPanels = new ArrayList<>();
        for(int i = 0; i < game.getMovements(); i++){
            StackPanel aux = new StackPanel(game,i,game.getBoard().getDimension() * CELL_SIZE);
            aux.setLocation(game.getBoard().getDimension() * CELL_SIZE + 80 + i*StackPanel.PANEL_WIDTH,0);
            stackPanels.add(aux);
            add(aux);
        }
    }
}
