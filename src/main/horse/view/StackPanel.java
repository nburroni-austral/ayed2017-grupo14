package main.horse.view;

import main.horse.controller.BoardListener;
import main.horse.model.DynamicStack;
import main.horse.controller.Game;
import main.horse.model.Location;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Brian Froschauer on 3/4/2017.
 */
public class StackPanel extends JPanel implements BoardListener {

    private Game game;
    public static final int TEXT_HEIGHT = 50;
    public static final int PANEL_WIDTH = 100;
    private int index;
    private int height;

    public StackPanel(Game game, int index, int height) {
        this.game = game;
        this.index = index;
        this.height = height;

        setLayout(null);
        setSize(PANEL_WIDTH, height);
        setBackground(Color.lightGray);
        game.getBoard().addBoardListener(this);
    }

    @Override
    public void boardUpdated(ArrayList<Location> positions) {
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);
        DynamicStack<Location> stack = game.getStacks().get(index);
        int i = 1;
        g.drawString("Stack " + (index+1), 20, height - TEXT_HEIGHT * i);
        while (!stack.isEmpty()) {
            i++;
            g.drawString(stack.peek().toString(), 30, height - TEXT_HEIGHT * i);
            stack.pop();
        }
    }
}
