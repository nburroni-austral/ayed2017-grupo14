package main.horse.view;

import main.horse.controller.BoardListener;
import main.horse.controller.Game;
import main.horse.model.Location;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Brian Froschauer on 3/4/2017.
 */
public class BoardPanel extends JPanel implements BoardListener {

    private static final long serialVersionUID = 1L;
    private int cellSize;
    private int rows, columns;
    private Image[][] cells;
    private Game game;
    private Image darkSquare;
    private Image lightSquare;
    private Image redDot;

    private ArrayList<Location> lastPositions; // Lista de posiciones

    public BoardPanel(final int rows, final int columns, final int cellSize, Game game) {
        this.rows = rows;
        this.columns = columns;
        this.cellSize = cellSize;
        this.cells = new Image[rows][columns];
        this.game = game;

        lastPositions = new ArrayList<>();
        loadImages();
        initCells();

        game.getBoard().addBoardListener(this);

        setSize(columns * cellSize + 1, rows * cellSize + 1);
        setBackground(Color.DARK_GRAY);
    }

    private void loadImages(){
        try {
            darkSquare = ImageIO.read(new File("src/main/horse/black.png"));
            lightSquare = ImageIO.read(new File("src/main/horse/white.png"));
            redDot = ImageIO.read(new File("src/main/horse/green.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initCells(){
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                setImage(i,j,(i+j)%2==0?darkSquare:lightSquare);
            }
        }
    }

    private void resetCell(int i, int j){

        clearImage(i,j);
        setImage(i,j,(i+j)%2==0?darkSquare:lightSquare);
    }

    public void clearImage(int row, int column) {
        cells[row][column] = null;
    }

    public void setImage(int row, int column, Image image) {
        cells[row][column] = new BufferedImage(cellSize, cellSize, BufferedImage.TYPE_INT_ARGB);
        cells[row][column].getGraphics().drawImage(image, 0, 0, null);
        repaint();
    }

    public void appendImage(int row, int column, Image image) {
        if (cells[row][column] == null) {
            cells[row][column] = new BufferedImage(cellSize, cellSize, BufferedImage.TYPE_INT_ARGB);
        }
        cells[row][column].getGraphics().drawImage(image, 0, 0, null);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int x = game.getStart().getX();
        int y = game.getStart().getY();
        cells[x][y].getGraphics()
                .drawImage(game.getPieceImage().getScaledInstance(cellSize,cellSize,Image.SCALE_DEFAULT)
                        ,0,0,null);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (cells[i][j] != null) {
                    g.drawImage(cells[i][j], j * cellSize, i * cellSize, null);
                }
            }
        }

        Location start = game.getStart();
        for(int i = 0; i < lastPositions.size();i++){
            Location finish = lastPositions.get(i);
            PaintUtils.drawArrowLine(g, cellSize * start.getY() + (cellSize / 2), cellSize * start.getX() + (cellSize / 2),
                    cellSize * finish.getY() + (cellSize / 2), cellSize * finish.getX() + (cellSize / 2)
                    , 20, 7);
            start = finish;
        }
    }

    @Override
    public void boardUpdated(ArrayList<Location> positions) {
        for(Location l: lastPositions){
            resetCell(l.getX(), l.getY());
        }
        lastPositions.clear();
        for(Location l: positions){
            lastPositions.add(l);
        }
        for(Location p: positions){
            resetCell(p.getX(),p.getY());
            appendImage(p.getX(),p.getY(),redDot);
        }
        paintImmediately(getBounds());
    }
}
