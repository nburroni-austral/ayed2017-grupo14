package main.horse.view;

import main.horse.controller.Game;
import javax.swing.*;
import java.io.IOException;

/**
 * Created by Brian Froschauer on 3/4/2017.
 */
public class Simulator {

    public static void main(String[] args) {
        Game game = new Game(4);
        BoardFrame frame = null;
        try {
            frame = new BoardFrame(game);
        } catch (IOException e) {
            e.printStackTrace();
        }
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
