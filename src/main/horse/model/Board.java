package main.horse.model;

import main.horse.controller.BoardListener;

import java.util.ArrayList;

/**
 * Created by Brian Froschauer on 31/3/2017.
 */
public class Board {

    private int dimension = 8;
    private Location[][] board;

    private ArrayList<Location> lastLocations;
    private ArrayList<Location> markedLocations;
    private ArrayList<BoardListener> listeners;

    public Board() {
        board = new Location[dimension][dimension];
        lastLocations = new ArrayList<>();
        markedLocations = new ArrayList<>();
        listeners = new ArrayList<>();
        fillBoard();
    }

    /*
     * LLeno el tablero con Locations.
     */
    public void fillBoard() {
        for(int i = 0; i < dimension; i++){
            for(int j = 0; j < dimension; j++){
                board[i][j] = new Location(i, j);
            }
        }
    }

    public boolean isValidPosition(Location location){
        for(int i = 0; i < dimension; i++){
            for(int j = 0; j < dimension;j++){
                if(board[i][j].equals(location)){
                    return true;
                }
            }
        }
        return false;
    }

    public void mark(DynamicStack<Location> locations){
        Location location = null;
        unmark();

        while (!locations.isEmpty()){
            location = locations.peek();
            board[location.getX()][location.getY()].setMarked(true);
            locations.pop();
            markedLocations.add(location);
        }
        lastLocations.add(location);
        onBoardUpdated();
    }

    public void unmark(){
        for (Location location: markedLocations){
            location.setMarked(false);
        }
        markedLocations.clear();
    }

    public void addBoardListener(BoardListener listener){
        listeners.add(listener);
    }

    private void onBoardUpdated(){
        for(BoardListener listener: listeners){
            listener.boardUpdated(markedLocations);
        }
    }

    public int getDimension() {
        return dimension;
    }

    public ArrayList<Location> getLastLocation() {
        return lastLocations;
    }
}
