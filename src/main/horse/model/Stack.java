package main.horse.model;

public interface Stack<T> {
    void push(T t);
    void pop();
    T peek();
    boolean isEmpty();
    int size();
    void empty(); // Delete all elements of stack
}
