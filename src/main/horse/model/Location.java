package main.horse.model;

/**
 * Created by Brian Froschauer on 31/3/2017.
 */
public class Location {

    private int x, y;
    private boolean marked;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
        marked = false;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
