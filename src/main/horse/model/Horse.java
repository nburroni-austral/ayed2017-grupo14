package main.horse.model;

/**
 * Created by Brian Froschauer on 31/3/2017.
 */
public class Horse {

    private Location location;

    public Horse(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public DynamicStack<Location> search(int x, int y) {
        DynamicStack<Location> possibleMovements = new DynamicStack<>();

        if (checkBounds(x+1, y+2)){
            Location movement = new Location(x+1, y+2);
            possibleMovements.push(movement);
        }

        if (checkBounds(x+1, y-2)){
            Location movement = new Location(x+1, y-2);
            possibleMovements.push(movement);
        }

        if(checkBounds(x-1, y+2)){
            Location movement = new Location(x-1, y+2);
            possibleMovements.push(movement);
        }

        if(checkBounds(x-1, y-2)){
            Location movement = new Location(x-1, y-2);
            possibleMovements.push(movement);
        }

        if(checkBounds(x+2, y+1)){
            Location movement = new Location(x+2, y+1);
            possibleMovements.push(movement);
        }

        if(checkBounds(x-2, y+1)){
            Location movement = new Location(x-2, y+1);
            possibleMovements.push(movement);
        }

        if(checkBounds(x+2, y-1)){
            Location movement = new Location(x+2, y-1);
            possibleMovements.push(movement);
        }

        if(checkBounds(x-2, y-1)){
            Location movement = new Location(x-2, y-1);
            possibleMovements.push(movement);
        }
        return possibleMovements;
    }

    private boolean checkBounds(int x, int y) {
        return (x >= 0 && x < 8 && y >= 0 && y < 8);
    }

    /**
     * Funcionamiento:
     * Se crea un STACK de locations,
     * Al stack pasado, mientras este lleno, se chequea si el ultimo elemento es una Location valida,
     * de ser asi la agrega al nuevo stacks, es decir filtra las locations, que no halla alguna igual en el tablero.
     *
     * @param board se le pasa un tablero
     * @param movements es una pila de Location llamada movements
     * @return una Pila de Location con posiciones validas
     */

    protected DynamicStack<Location> filterMovements(Board board, DynamicStack<Location> movements){
        DynamicStack<Location> validLocation = new DynamicStack<>();

        while (!movements.isEmpty()){
            if(board.isValidPosition(movements.peek())){
                validLocation.push(movements.peek());
            }
            movements.pop();
        }

        return validLocation;
    }
}
