package main.horse.controller;

import main.horse.model.Location;

import java.util.ArrayList;

/**
 * Created by Brian Froschauer on 3/4/2017.
 */
public interface BoardListener {
    void boardUpdated(ArrayList<Location> positions);
}
