package main.horse.controller;

import main.horse.model.Board;
import main.horse.model.DynamicStack;
import main.horse.model.Horse;
import main.horse.model.Location;
import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Brian Froschauer on 31/3/2017.
 */
public class Game {

    private ArrayList<DynamicStack<Location>> stacks;
    private Board board;
    private Horse horse;
    private Location start;
    private int movements;
    private boolean finished;
    private Image pieceImage;

    public Game(int movements) {
        stacks = new ArrayList<>();
        board = new Board();
        start = new Location(0, 0);
        horse = new Horse(start);
        this.movements = movements;
        finished = false;

        try {
            pieceImage = ImageIO.read(new File("src/main/horse/horse.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        fillArray();

        // Busco posibles movimientos para la posicion actual del caballo que seria start
        stacks.set(0, horse.search(horse.getLocation().getX(), horse.getLocation().getY()));
        fillStacks();

    }

    /*
     * Add four stacks to ArrayList<Stack>
     */
    public void fillArray() {
        for (int i = 0; i < movements; i++) {
            stacks.add(new DynamicStack<>());
        }
    }

    /*
     * Busco a partir del segundo stack del ArrayList,
     * si esta vacio, lo cual es true, seteo la posicion del caballo al ultimo elemento del primer stack
     * luego lleno el segundo stack con los posibles movimientos respecto a esa posicion
     */
    public void fillStacks(){
        for (int i = 1 ; i < stacks.size(); i++) {
            if (stacks.get(i).isEmpty()) {
                horse.setLocation(stacks.get(i - 1).peek());
                stacks.set(i, horse.search(horse.getLocation().getX(), horse.getLocation().getY()));
            }
        }
    }

    public DynamicStack<Location> nextMovement(){
        DynamicStack<Location> result = new DynamicStack<>();

        if (stacks.get(0).isEmpty()) return null; // No more movements

        for (int i = stacks.size()-1; i >= 0; i--) {
            result.push(stacks.get(i).peek()); // Add to result the first elements of all stacks.
        }

        for (int j = stacks.size()-1; j >= 0; j--) {
            stacks.get(j).pop(); // Pop first Location of the all stacks.

            if (!stacks.get(j).isEmpty()){
                break;
            }
        }

        fillStacks();
        return result;
    }

    public void next(){
        DynamicStack<Location> locations = nextMovement();

        if(locations == null){
            System.out.println("No more movements.");
            finished = true;
        }
        board.mark(locations);
    }

    public ArrayList<DynamicStack<Location>> getStacks() {
        ArrayList<DynamicStack<Location>> result = new ArrayList<>();
        for(DynamicStack<Location> stack : stacks){
            result.add(copy(stack));
        }
        return result;
    }

    /*
     * Copy the passed stack and return it.
     */

    public DynamicStack<Location> copy(DynamicStack<Location> stack){
        DynamicStack<Location> locations = new DynamicStack<>();

        while (!stack.isEmpty()){
            locations.push(stack.peek());
            stack.pop();
        }

        DynamicStack<Location> result = new DynamicStack<>();

        while (!locations.isEmpty()){
            result.push(locations.peek());
            stack.push(locations.peek());
            locations.pop();
        }
        return result;
    }

    public Board getBoard() {
        return board;
    }

    public Location getStart() {
        return start;
    }

    public int getMovements() {
        return movements;
    }

    public Image getPieceImage() {
        return pieceImage;
    }
}
