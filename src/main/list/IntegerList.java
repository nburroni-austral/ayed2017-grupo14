package main.list;

/**
 * Created by matiasvenditti on 4/19/17.
 */
public class IntegerList extends StaticList<Integer> {
    private int counter;

    public IntegerList(){
        super();
        counter = 0;
    }

    private void returnSum(){
        this.counter = 0;
        for(int i = 0; i < this.size(); i++){
            this.goTo(i);
            counter += this.getActual();
        }
    }

    public void insert(Integer obj){
        if (obj == 0){
            returnSum();
            System.out.println(this.counter);
        }
        else{
            this.insertNext(obj);
        }
    }

    public int getCounter() {
        return counter;
    }
}
