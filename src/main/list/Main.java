package main.list;


import struct.istruct.list.GeneralList;

/**
 * Created by matiasvenditti on 4/19/17.
 */
public class Main {

    public static void main(String[] args) {
        IntegerList list = new IntegerList();
        list.insert(1);
        System.out.println("Inserting 1 Counter = " + list.getCounter());
        list.insert(2);
        System.out.println("Inserting 2 Counter = " + list.getCounter());
        list.insert(3);
        System.out.println("Inserting 3 Counter = " + list.getCounter());
        list.insert(4);
        System.out.println("Inserting 4 Counter = " + list.getCounter());
        list.insert(5);
        System.out.println("Inserting 5 Counter = " + list.getCounter());
        list.insert(0);
        System.out.println("Inserting 0 Counter = " + list.getCounter());

        int result = 0;
        result += 1;
        result += 2;
        result += 3;
        result += 4;
        result += 5;

        System.out.println("Result: " + result);


    }
}
