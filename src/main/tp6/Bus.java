package main.tp6;


/**
 * Created by matiasvenditti on 5/15/17.
 */
public class Bus implements Comparable<Bus>{

    private int lineNumber;
    private int homeNumber;
    private int seats;
    private boolean isSuitableForDisabled;

    public Bus(int lineNumber, int homeNumber, int seats, boolean isSuitableForDisabled) {
        this.lineNumber = lineNumber;
        this.homeNumber = homeNumber;
        this.seats = seats;
        this.isSuitableForDisabled = isSuitableForDisabled;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int getHomeNumber() {
        return homeNumber;
    }

    public int getSeats() {
        return seats;
    }

    public boolean isSuitableForDisabled() {
        return isSuitableForDisabled;
    }

    @Override
    public int compareTo(Bus o) {
        if (this.lineNumber == o.getLineNumber()){
            return this.homeNumber - o.getHomeNumber();
        }
        else{
            return this.lineNumber - o.getLineNumber();
        }
    }

}
