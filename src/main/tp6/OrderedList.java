package main.tp6;

import main.list.StaticList;
import struct.istruct.list.SortedList;
import java.io.Serializable;

/**
 * Created by matiasvenditti on 5/15/17.
 */
public class OrderedList<T extends Comparable<T>> extends StaticList<T> implements SortedList<T>, Serializable{

    @Override
    public void insert(T obj) {
        if (size() == 0 || obj.compareTo(getActual()) == 0){
            insertNext(obj);
        }
        else if (obj.compareTo(getActual()) > 0){
            goForward(obj);
        }
        else{
            goBackward(obj);
        }
    }

    private void goForward(T obj){
        while(next()){
            if (obj.compareTo(getActual()) < 0){
                insertPrev(obj);
                return;
            }
        }
        insertNext(obj);
    }

    private void goBackward(T obj){
        while(prev()){
            if (obj.compareTo(getActual()) > 0){
                insertNext(obj);
                return;
            }
        }
        insertPrev(obj);
    }

    private boolean next(){
        if (getActualPosition() == size() - 1){
            return false;
        }
        else{
            goNext();
            return true;
        }

    }

    private boolean prev(){
        if (getActualPosition() == 0){
            return false;
        }
        else{
            goPrev();
            return true;
        }
    }
}
