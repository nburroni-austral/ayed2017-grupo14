package main.tp6;


import java.io.*;
import java.util.Scanner;

/**
 * Created by matiasvenditti on 5/15/17.
 */
public class Simulation {

    public void run(){
        boolean value = true;
        OrderedList<Bus> buses = new OrderedList<>();
        Scanner scanner = new Scanner(System.in);
        while(value){
            System.out.println("1. To add a bus\n2. To delete a bus\n3. To get report\n4. To get amount of suitable for disabled buses\n5. To get amount of buses with more than 27 seats\n6. To save into a file\n7. To get from a file\n8. To exit.");
            int command = scanner.nextInt();
            switch (command){
                case 1:
                    insertBus(createBusFromUserInput(), buses);
                    System.out.println("Buses list size: " + buses.size());
                    break;
                case 2:
                    System.out.println(getKeys(buses));
                    System.out.println("Choose a line number: ");
                    int lineNumber = scanner.nextInt();
                    System.out.println("Choose a home number: ");
                    int homeNumber = scanner.nextInt();
                    deleteBus(lineNumber, homeNumber, buses);
                    System.out.println("");
                    System.out.println("Buses list size: " + buses.size());
                    break;
                case 3:
                    System.out.println(getReport(buses));
                    break;
                case 4:
                    OrderedList<Bus> capableBuses = getCapableLines(buses);
                    if (capableBuses.size() != 0){
                        System.out.println("Amount of buses capable of transporting disabled: " + capableBuses.size() + "\n");
                        System.out.println(getReport(capableBuses));
                    }
                    else{
                        System.out.println("Buses capable of transporting disabled could not be found.");
                    }
                    break;
                case 5:
                    OrderedList<Bus> list = getAmountOfSeatsGreaterThanX(buses, 27);
                    if (list.size() != 0){
                        System.out.println("\nAmount of buses with more than 27 seats: " + list.size()+ "\n");
                        System.out.println(getReport(list));
                    }
                    else{
                        System.out.println("Buses with more than 27 seats could not be found.");
                    }
                    break;
                case 6:
                    System.out.println("Insert file name: ");
                    String fileName = scanner.next();
                    serialize(fileName, buses);
                    break;
                case 7:
                    System.out.println("Insert file name: ");
                    String file = scanner.next();
                    try{
                        System.out.println("...Recovering sorted list of buses...");
                        System.out.println(getReport(deserialize(file)));
                    }
                    catch (IOException e){
                        System.out.println(e.getMessage());
                    }
                    catch (ClassNotFoundException c){
                        c.printStackTrace();
                    }
                    break;
                case 8:
                    value = false;
                    break;
            }
        }
    }

    private Bus createBusFromUserInput(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert bus line number: ");
        int lineNumber = scanner.nextInt();
        System.out.println("Insert bus home number: ");
        int homeNumber = scanner.nextInt();
        System.out.println("Insert amount of seats: ");
        int amountOfSeats = scanner.nextInt();
        System.out.println("Is suitable for disabled: ");
        boolean isSuitableForDisabled = scanner.nextBoolean();
        return new Bus(lineNumber, homeNumber, amountOfSeats, isSuitableForDisabled);
    }

    private void insertBus(Bus bus, OrderedList<Bus> buses){
        buses.insert(bus);
    }

    private String getReport(OrderedList<Bus> buses){
        String result = "" + "\n-------------------------REPORT-STARTING-------------------------";
        for (int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            Bus actual = buses.getActual();
            result += "\nBus number " + (i+1) + "\nLine number: " + actual.getLineNumber() + "\nHome number: " + actual.getHomeNumber() + "\nNumber of seats: " + actual.getSeats() + "\nIs suitable for disabled: " + actual.isSuitableForDisabled() + "\n";
        }
        result += "\n-------------------------REPORT-FINALIZING-------------------------";
        return result;
    }

    private void deleteBus(int lineNumber, int homeNumber, OrderedList<Bus> buses){
        for (int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            if (buses.getActual().getLineNumber() == lineNumber && buses.getActual().getHomeNumber() == homeNumber){
                buses.remove();
                return;
            }
        }
        System.out.println("No such key found in the list.");
    }

    private String getKeys(OrderedList<Bus> buses){
        String keys = "";
        for (int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            keys += "[Line number: " + buses.getActual().getLineNumber() + ", Home number: " + buses.getActual().getHomeNumber() + "]\n";
        }
        return keys;
    }


    private OrderedList<Bus> getCapableLines(OrderedList<Bus> buses){
        OrderedList<Bus> busesResult = new OrderedList<>();
        for (int i = 0; i < buses.size(); i++){
            buses.goTo(i);
            if (buses.getActual().isSuitableForDisabled()){
                busesResult.insert(buses.getActual());
            }
        }
        return busesResult;
    }

    private OrderedList<Bus> getAmountOfSeatsGreaterThanX(OrderedList<Bus> buses, int x){
        OrderedList<Bus> busesResult = new OrderedList<>();
        for (int i = 0; i < buses.size(); i++) {
            buses.goTo(i);
            if (buses.getActual().getSeats() > x){
                busesResult.insert(buses.getActual());
            }
        }
        return busesResult;
    }


    private void serialize(String fileName, OrderedList<Bus> buses){
        try{
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
            oos.writeObject(buses);
            oos.close();
            System.out.println("Successfully saved into file " + fileName);
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private OrderedList<Bus> deserialize(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
        return (OrderedList<Bus>)ois.readObject();
    }

}
