package main.tp8;

import main.binarySearchTree.BinarySearchTree;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by matiasvenditti on 4/17/17.
 */
public class Stock{

    private BinarySearchTree<Lamp> tree;

    /**
     * Creates a stock of lamps.
     * @param list of lamps.
     */
    public Stock(LinkedList<Lamp> list){
        tree = new BinarySearchTree<>();
        copyValuesToTree(list);
    }

    /**
     * Copies the values passed as a linked list to a binary search tree.
     * @param list of values to be copied.
     */
    private void copyValuesToTree(LinkedList<Lamp> list){
        BinarySearchTree<Lamp> result = new BinarySearchTree<>();
        for (Lamp lamp : list){
            result.insert(lamp);
        }
        tree = result;
    }

    /**
     * Prints the lamp's codes in list format.
     */
    public void printDisplay(){
        ArrayList<Lamp> result = tree.display();
        ArrayList<String> result2 = new ArrayList();
        for(Lamp lamp : result){
            result2.add(lamp.getLampCode());
        }
        System.out.println(result2.toString());
    }

    public void insert(Lamp lamp){
        tree.insert(lamp);
    }

    public void delete(Lamp lamp){
        tree.delete(lamp);
    }

    public void modify(Lamp lamp){
        Lamp result = tree.search(lamp);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert command\n1. To set new lamp code\n2. To set new lamp amount.\n3. To set new lamp type.\n4. To set new watts equivalent.");
        int value = scanner.nextInt();
        switch (value){
            case 1:
                System.out.println("Insert new lamp code: ");
                String code = scanner.next();
                Lamp aux = new Lamp(code, result.getWatts(), result.getLampType(), result.getAmount());
                tree.delete(result);
                tree.insert(aux);
                break;
            case 2:
                System.out.println("Insert new amount: ");
                int amount = scanner.nextInt();
                result.setAmount(amount);
                break;

            case 3:
                System.out.println("Insert new lamp type: ");
                String type = scanner.next();
                result.setLampType(type);
                break;

            case 4:
                System.out.println("Insert new watts value: ");
                int watts = scanner.nextInt();
                result.setWatts(watts);
                break;
            default:
                return;
        }
    }
}
