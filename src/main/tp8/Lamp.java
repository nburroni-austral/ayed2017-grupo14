package main.tp8;

import java.util.IllegalFormatException;

/**
 * Created by matiasvenditti on 4/17/17.
 */
public class Lamp implements Comparable<Lamp>{

    private String lampCode;
    private int watts;
    private String lampType;
    private int amount;

    public Lamp(String lampCode, int watts, String lampType, int amount){
        checkLampCode(lampCode);
        this.watts = watts;
        checkLampType(lampType);
        this.amount = amount;
    }

    public String getLampCode() {
        return lampCode;
    }

    public int getWatts() {
        return watts;
    }

    public String getLampType() {
        return lampType;
    }

    public int getAmount() {
        return amount;
    }

    public void setWatts(int watts) {
        this.watts = watts;
    }

    public void setLampType(String lampType) {
        this.lampType = lampType;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    private void checkLampCode(String lampCode){
        if (lampCode.length() > 5){
            throw new RuntimeException("Lamp code: '" + lampCode + "' is too long.");
        }
        else{
            this.lampCode = lampCode;
        }
    }

    private void checkLampType(String lampType){
        if (lampType.length() > 10){
            throw new RuntimeException("Lamp type of lamp '" + lampCode + "' name is too long.");
        }
        else {
            this.lampType = lampType;
        }
    }

    @Override
    public int compareTo(Lamp o) {
        return lampCode.compareTo(o.getLampCode());
    }
}
