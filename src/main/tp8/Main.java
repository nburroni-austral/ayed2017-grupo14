package main.tp8;

import java.util.LinkedList;

/**
 * Created by matiasvenditti on 4/17/17.
 */
public class Main {

    public static void main(String[] args) {
        Lamp l1 = new Lamp("1",20, "Bath", 287);
        Lamp l2 = new Lamp("3",20, "Car", 357);
        Lamp l3 = new Lamp("6",20, "Desk", 140);
        Lamp l4 = new Lamp("4",20, "Pool", 220);
        Lamp l5 = new Lamp("5",20, "Red", 305);
        Lamp l6 = new Lamp("2",20, "Blue", 423);

        System.out.println("Creating random lamps with different lamp codes.");
        LinkedList<Lamp> list = new LinkedList<>();
        list.add(l1);
        list.add(l2);
        list.add(l3);
        list.add(l4);
        list.add(l5);
        list.add(l6);
        System.out.println("Adding the lamps to the 'old' structure with the use of a linked list.");

        Stock stock = new Stock(list);
        System.out.println("New organization of data using binary search tree.");
        System.out.println("Lamp codes in the binary search tree put in order as a list.");
        System.out.println("The binary search tree is transversed with inorder technique.");
        stock.printDisplay();

        System.out.println("Adding a new lamp to the stock.");
        stock.insert(new Lamp("9", 20, "Green", 140));
        stock.printDisplay();

        System.out.println("Removing an old lamp from the stock.");
        stock.delete(l1);
        stock.printDisplay();

        System.out.println("Modifying an existing value from the stock.");
        stock.modify(l3);
        stock.printDisplay();


    }
}
