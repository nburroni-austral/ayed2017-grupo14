package main.tp10;

import java.io.FileReader;
import java.io.IOException;

/**
 * Created by matiasvenditti on 6/16/17.
 */
public class Exercise2 {

    public static int getOcurrenciesAmount(char character, String fileName){
        int counter = 0;
        try{
            FileReader fr = new FileReader(fileName);
            int read = fr.read();
            while(read != -1){
                if (character == read){
                    counter++;
                }
                read = fr.read();
            }
            return counter;

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        return -1;
    }
}
