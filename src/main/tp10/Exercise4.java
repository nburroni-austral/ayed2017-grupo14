package main.tp10;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by matiasvenditti on 6/16/17.
 */
public class Exercise4 {

    //EXERCISE 4

    @SuppressWarnings("Duplicates")
    public static void exercise4(String fileName, String firstFile, String secondFile){
        try{
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            ArrayList<String> greaterPopulation = new ArrayList<>();
            ArrayList<String> littlePopulation = new ArrayList<>();
            String read = br.readLine();
            while(read != null){
                if (Float.parseFloat(getPopulation(read)) < 30){
                    littlePopulation.add(read);
                }
                else{
                    greaterPopulation.add(read);
                }
                read = br.readLine();
            }

            generateAssociatedFile(greaterPopulation, firstFile);
            generateAssociatedFile(littlePopulation, secondFile);

            br.close();

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static String getPopulation(String data){
        int index1 = 0;
        int index2 = 0;
        for (int i = 0; i < data.length(); i++){
            if (data.charAt(i) == ' ' && index1 == 0){
                index1 = i;
            }
            else if (data.charAt(i) == ' '){
                index2 = i;
            }
        }

        return data.substring(index1, index2);
    }

    private static void generateAssociatedFile(ArrayList<String> countries, String fileName){
        try{
            if (countries.size() != 0){
                BufferedWriter bw1 = new BufferedWriter(new FileWriter(fileName));
                for (String s : countries){
                    bw1.write(s + "\n");
                }
                bw1.close();
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }


    //EXERCISE 5

    public static void exercise5(String fileName, String firstFile, String secondFile, String option){
        switch (option){
            case "PBI":
                pbi(fileName, firstFile, secondFile);
                break;
            case "POB":
                pob(fileName, firstFile, secondFile);
                break;
            default:
                exercise4(fileName, firstFile, secondFile);
        }
    }

    @SuppressWarnings("Duplicates")
    private static void pbi(String fileName, String firstFile, String secondFile){
        try{
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            ArrayList<String> greaterPopulation = new ArrayList<>();
            ArrayList<String> littlePopulation = new ArrayList<>();
            String read = br.readLine();
            while(read != null){
                if (Float.parseFloat(getPopulation(read)) < 30){
                    littlePopulation.add(getCountryAndPBI(read));
                }
                else{
                    greaterPopulation.add(getCountryAndPBI(read));
                }
                read = br.readLine();
            }
            generateAssociatedFile(greaterPopulation, firstFile);
            generateAssociatedFile(littlePopulation, secondFile);

            br.close();

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static void pob(String fileName, String firstFile, String secondFile){
        try{
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            ArrayList<String> greaterPopulation = new ArrayList<>();
            ArrayList<String> littlePopulation = new ArrayList<>();
            String read = br.readLine();
            while(read != null){
                if (Float.parseFloat(getPopulation(read)) < 30){
                    littlePopulation.add(getCountryAndPopulation(read));
                }
                else{
                    greaterPopulation.add(getCountryAndPopulation(read));
                }
                read = br.readLine();
            }
            generateAssociatedFile(greaterPopulation, firstFile);
            generateAssociatedFile(littlePopulation, secondFile);

            br.close();

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static String getCountryAndPBI(String data){
        String result = "";
        int index = 0;
        for (int i = 0; i < data.length(); i++){
            if (data.charAt(i) == ' '){
                index = i;
                break;
            }
        }
        result += data.substring(0, index);

        for (int i = index+1; i < data.length(); i++){
            if (data.charAt(i) == ' '){
                result += data.substring(i, data.length());
                break;
            }
        }
        return result;
    }

    private static String getCountryAndPopulation(String data){
        int index = 0;
        for (int i = 0; i < data.length(); i++){
            if (data.charAt(i) == ' '){
                index = i;
                break;
            }
        }
        String result = data.substring(0, index);
        result += " " + getPopulation(data);
        return result;
    }
}
