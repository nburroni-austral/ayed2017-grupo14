package main.tp10;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by matiasvenditti on 6/16/17.
 */
public class Main {

    public static void main(String[] args) {

        try{
            FileWriter fw = new FileWriter("Test");
            fw.write('1');
            fw.write('2');
            fw.write('3');
            fw.write('a');
            fw.write('A');
            fw.write('B');
            fw.write('a');
            fw.write('a');
            fw.write('Z');
            fw.write('9');
            fw.write('a');
            fw.write('a');
            fw.close();

//             Exercise 1 test.

//            System.out.println(Exercise1.getCharAmount('C', "Test"));
//            System.out.println(Exercise1.getLineAmount('L', "Test"));

            // Exercise 2 test.

//            System.out.println("Amount of 'a' in file: " + Exercise2.getOcurrenciesAmount('a', "Test"));

            //Exercise 3 test.

//            Exercise3.copyToFile("Test", "Copy");

            BufferedWriter bw = new BufferedWriter(new FileWriter("Test"));
            bw.write("Argentina 43.42 583.2\n");
            bw.write("Chile 17.95 240,2\n");
            bw.write("Brazil 207.8 1775\n");
            bw.write("Brazil 48.23 1144\n");
            bw.close();

            Exercise4.exercise5("Test", "File1", "File2", "PBI");
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
