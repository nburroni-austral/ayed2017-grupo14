package main.tp10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by matiasvenditti on 6/16/17.
 */
public class Exercise1 {

    public static String getCharAmount(char character, String fileName){
        int counter = 0;
        try{
            if (character == 'C'){
                FileReader fr = new FileReader(fileName);
                int read = fr.read();
                while(read != -1){
                    counter++;
                    read = fr.read();
                }
                fr.close();
            }

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        return "Amount of characters: " + counter;
    }

    public static String getLineAmount(char character, String fileName){
        int counter = 0;
        try{
            if (character == 'L'){
                BufferedReader br = new BufferedReader(new FileReader(fileName));
                String read = br.readLine();
                while(read != null){
                    counter++;
                    read = br.readLine();
                }
                br.close();
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        return "Amount of lines: " + counter;
    }
}
