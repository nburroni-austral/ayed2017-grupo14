package main.tp10;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by matiasvenditti on 6/16/17.
 */
public class Exercise3 {

    public static void copyToFile(String fileName, String anotherName){
        try{
            FileReader fr = new FileReader(fileName);
            FileWriter fw = new FileWriter(anotherName);
            int read = fr.read();
            while(read != -1){
                read = (Character.isUpperCase(read)? Character.toLowerCase(read) : Character.toUpperCase(read));
                fw.write(read);
                read = fr.read();
            }
            fr.close();
            fw.close();

        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
