package main.binaryTree;
import java.io.*;

/**
 * Created by Brian Froschauer on 4/4/2017.
 */
public class Test {

    public static void main(String[] args) throws IOException {

        // TREE A OF INTEGER
        BinaryTree<Integer> treeA1 = new BinaryTree<>(10);
        BinaryTree<Integer> treeA2 = new BinaryTree<>(3);
        BinaryTree<Integer> treeA3 = new BinaryTree<>(0);
        BinaryTree<Integer> treeA4 = new BinaryTree<>(7, treeA1, treeA2);
        BinaryTree<Integer> treeA5 = new BinaryTree<>(4, new BinaryTree<>(), treeA3);
        BinaryTree<Integer> treeA6 = new BinaryTree<>(8);
        BinaryTree<Integer> treeA7 = new BinaryTree<>(4, treeA4, new BinaryTree<>());
        BinaryTree<Integer> treeA8 = new BinaryTree<>(9);
        BinaryTree<Integer> treeA9 = new BinaryTree<>(1, treeA5, treeA6);
        BinaryTree<Integer> treeA10 = new BinaryTree<>(2, treeA7, treeA8);
        BinaryTree<Integer> treeA = new BinaryTree<>(5, treeA9, treeA10);

        // TREE B OF INTEGER
        BinaryTree<Integer> treeB1 = new BinaryTree<>(9);
        BinaryTree<Integer> treeB2 = new BinaryTree<>(2);
        BinaryTree<Integer> treeB3 = new BinaryTree<>(7);
        BinaryTree<Integer> treeB4 = new BinaryTree<>(5, treeB1, treeB2);
        BinaryTree<Integer> treeB5 = new BinaryTree<>(8, new BinaryTree<>(), treeB3);
        BinaryTree<Integer> treeB6 = new BinaryTree<>(8);
        BinaryTree<Integer> treeB7 = new BinaryTree<>(12, treeB4, new BinaryTree<>());
        BinaryTree<Integer> treeB8 = new BinaryTree<>(3);
        BinaryTree<Integer> treeB9 = new BinaryTree<>(1, treeB5, treeB6);
        BinaryTree<Integer> treeB10 = new BinaryTree<>(2, treeB7, treeB8);
        BinaryTree<Integer> treeB = new BinaryTree<>(6, treeB9, treeB10);

        TreeAplication app = new TreeAplication();

        System.out.println("ALTURA: " + app.getHeight(treeA)); // 5
        System.out.println("HOJAS: " + app.getLeafs(treeA)); // 5
        System.out.println("HOJAS EN NIVEL 3: " + app.getLeafs(treeA, 3)); // 1
        System.out.println("CANTIDAD DE APARICIONES DE 4: " + app.getNumber(treeA, 4)); // 2
        System.out.println("PESO: " + app.getWeight(treeA)); // 11

        System.out.println("SUM OF ELEMENTS: " + app.sumOfElements(treeA));
        System.out.println("SUM OF MULTIPLES OF THREE: " + app.sumOfMultiplesOfThree(treeA));
        System.out.println("EQUALS: " + app.equals(treeA, treeB));
        System.out.println("ISOMORPHIC: " + app.isomorphic(treeA, treeB));
        System.out.println("SIMILAR: " + app.similar(treeA, treeB));
        System.out.println("COMPLETE: " + app.complete(treeA));
        System.out.println("FULL: " + app.full(treeA));
        System.out.println("ESTABLE: " + app.estable(treeA));
        System.out.println("OCURRE BINARY TREE: " + app.ocurreArbin(treeA, treeB));

        System.out.println("--SHOW BORDER--");
        app.showBorder(treeA);
        app.border(treeA);
        System.out.println("--IN ORDER--");
        app.inOrder(treeA);
        System.out.println("--PRE ORDER--");
        app.preOrder(treeA);
        System.out.println("--POST ORDER--");
        app.postOrder(treeA);

        System.out.println("Testing serialization of a binary tree");
        serializeTree("tree.txt", treeA);
        try{
            BinaryTree tree = deserialize("tree.txt");
            boolean result = tree.equals(treeA);
            System.out.println(result);
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }


    }

    public static void serializeTree(String fileName, BinaryTree tree) throws IOException{
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
        oos.writeObject(tree);
    }

    public static BinaryTree deserialize(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
        return (BinaryTree)ois.readObject();

    }
}
