package main.binaryTree;

import main.queue.DynamicQueue;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Brian Froschauer on 4/4/2017.
 */
public class TreeAplication implements Serializable {

    public int getWeight(BinaryTree tree) {
        if (tree.isEmpty()) return 0;

        return 1 + getWeight(tree.getLeft()) + getWeight(tree.getRight());
    }

    public int getHeight(BinaryTree tree) {
        if (tree.isEmpty()) return 0;

        return 1 + Math.max(getHeight(tree.getLeft()), getHeight(tree.getRight()));
    }

    public int getLeafs(BinaryTree tree) {
        if (tree.isEmpty()) return 0;

        if (tree.getLeft().isEmpty() && tree.getRight().isEmpty()) return 1;

        return getLeafs(tree.getLeft()) + getLeafs(tree.getRight());
    }

    public int getNumber(BinaryTree tree, Object o) {
        if (tree.isEmpty())
            return 0;

        if (tree.getRoot().equals(o))
            return 1 + getNumber(tree.getLeft(), o) + getNumber(tree.getRight(), o);

        else
            return getNumber(tree.getLeft(), o) + getNumber(tree.getRight(), o);
    }

    public int getLeafs(BinaryTree tree, int level) {
        if (!tree.isEmpty()) {
            if (tree.getLeft().isEmpty() && tree.getRight().isEmpty()) {
                return (level == 0) ? 1 : 0;
            } else {
                return getLeafs(tree.getLeft(), level - 1) + getLeafs(tree.getRight(), level - 1);
            }
        }
        return 0;
    }


    public int sumOfElements(BinaryTree<Integer> tree) {
        if (!tree.isEmpty()) {
            return tree.getRoot() + sumOfElements(tree.getLeft()) + sumOfElements(tree.getRight());
        }
        return 0;
    }

    public int sumOfMultiplesOfThree(BinaryTree<Integer> a) {
        if (!a.isEmpty()) {
            if (a.getRoot() % 3 == 0) {
                return a.getRoot() + sumOfMultiplesOfThree(a.getLeft()) + sumOfMultiplesOfThree(a.getRight());
            } else {
                return sumOfMultiplesOfThree(a.getLeft()) + sumOfMultiplesOfThree(a.getRight());
            }
        }
        return 0;
    }

    public <T> boolean equals(BinaryTree<T> tree1, BinaryTree<T> tree2) {
        if (tree1 == tree2) {
            return true;
        }

        if (tree1.isEmpty() && tree2.isEmpty()) {
            return true;
        }

        if (tree1.isEmpty() && !tree2.isEmpty() || !tree1.isEmpty() && tree2.isEmpty()) {
            return false;
        }

        return tree1.getRoot().equals(tree2.getRoot()) && tree1.getLeft().equals(tree2.getLeft()) && tree1.getRight().equals(tree2.getRight());
    }

    public <T> boolean isomorphic(BinaryTree<T> tree1, BinaryTree<T> tree2) {
        if (tree1.isEmpty() && tree2.isEmpty()) {
            return true;
        }
        if (!tree1.isEmpty() && !tree2.isEmpty()) {
            return isomorphic(tree1.getLeft(), tree2.getLeft()) && isomorphic(tree1.getRight(), tree2.getRight());
        }
        return false;
    }

    public <T> boolean similar(BinaryTree<T> tree1, BinaryTree<T> tree2) {
        ArrayList<T> listA = new ArrayList<>();
        getElements(tree1, listA);
        ArrayList<T> listB = new ArrayList<>();
        getElements(tree2, listB);

        return listA.containsAll(listB) && listB.containsAll(listA);
    }

    private void getElements(BinaryTree tree, ArrayList list) {
        if (!tree.isEmpty()) {
            list.add(tree.getRoot());
            getElements(tree.getLeft(), list);
            getElements(tree.getRight(), list);
        }
    }

    public <T> boolean complete(BinaryTree<T> tree) {
        if (!tree.isEmpty()) {
            if (tree.getLeft().isEmpty() && tree.getRight().isEmpty()) return true;
            return complete(tree.getRight()) && complete(tree.getLeft());
        }
        return false;
    }

    public <T> boolean full(BinaryTree<T> a) {
        if (complete(a)) {
            int maxLevel = getHeight(a);
            int leavesLastLevel = getLeafs(a, maxLevel);
            return (leavesLastLevel == Math.pow(2, maxLevel));
        }
        return false;
    }

    public boolean estable(BinaryTree<Integer> tree) {
        if (tree.isEmpty()) {
            return true;
        }

        if (tree.getLeft().isEmpty() && tree.getRight().isEmpty()) {
            return true;
        }

        if (tree.getRoot() < tree.getLeft().getRoot() || tree.getRoot() < tree.getRight().getRoot()) {
            return false;
        }

        return estable(tree.getLeft()) && estable(tree.getRight());
    }

    public <T> boolean ocurreArbin(BinaryTree<T> tree1, BinaryTree<T> tree2) {
        if (tree2.isEmpty()) return true;

        if (tree1.isEmpty()) return false;

        if (equals(tree1, tree2)) return true;

        return ocurreArbin(tree1.getLeft(), tree2) || ocurreArbin(tree1.getRight(), tree2);
    }

    public <T> void showBorder(BinaryTree<T> tree) {
        ArrayList result = border(tree);
        System.out.println(result.toString());
    }

    public <T> ArrayList border(BinaryTree<T> tree) {
        ArrayList result = new ArrayList();
        border(tree, result);
        return result;
    }

    private <T> void border(BinaryTree<T> tree, ArrayList result) {
        if (!tree.isEmpty()) {
            if (tree.getLeft().isEmpty() && tree.getRight().isEmpty()) {
                result.add(tree.getRoot());
            }
            border(tree.getLeft(), result);
            border(tree.getRight(), result);
        }
    }

    public <T> void inOrder(BinaryTree<T> tree){

        if (!tree.isEmpty()){
            inOrder(tree.getLeft());
            System.out.println(tree.getRoot());
            inOrder(tree.getRight());
        }
    }

    public <T> void preOrder(BinaryTree<T> tree){
        if (!tree.isEmpty()){
            System.out.println(tree.getRoot());
            preOrder(tree.getLeft());
            preOrder(tree.getRight());
        }
    }

    public <T> void postOrder(BinaryTree<T> tree){
        if (!tree.isEmpty()){
            postOrder(tree.getLeft());
            postOrder(tree.getRight());
            System.out.println(tree.getRoot());
        }
    }

    public <T> void printByLevel(BinaryTree<T> tree) {
        BinaryTree<T> aux;
        DynamicQueue<BinaryTree<T>> queue = new DynamicQueue<>(50);

        if (!tree.isEmpty())queue.enqueue(tree);
        while (!queue.isEmpty()) {
            aux = queue.dequeue();
            if (!aux.getLeft().isEmpty()) {
                queue.enqueue(tree.getLeft());
                if (!aux.getRight().isEmpty()){
                    queue.enqueue(tree.getRight());
                }
            }
            System.out.println(aux.getRoot());
        }
    }

//    public void serializeBinary(BinaryTree tree) {
//        try {
//            File file = new File("tree.txt");
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//
//            ObjectOutputStream objectOut = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
//            objectOut.writeObject(tree);
//        } catch (Exception e) {
//            e.getMessage();
//        }
//    }
//
//    public BinaryTree deSerializeBinary(String file) {
//        try {
//            ObjectInputStream objectInput = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
//            Object obj = objectInput.readObject();
//            if (obj instanceof BinaryTree){
//                return (BinaryTree) obj;
//            }
//        } catch (Exception e){
//            e.getMessage();
//        }
//        throw new RuntimeException("Binary Tree not found");
//    }
}

