package main.binaryTree;

import java.io.Serializable;

/**
 * Created by Brian Froschauer on 4/4/2017.
 */
public class BinaryTree<T> implements Serializable{

    DoubleNode<T> root;

    public BinaryTree() {
        root = null;
    }

    public BinaryTree(T element) {
        root = new DoubleNode <>(element);
    }

    public BinaryTree(T element, BinaryTree<T> left, BinaryTree<T> right){
        root = new DoubleNode<>(element, left.root, right.root);
    }

    public boolean isEmpty() {
        return root == null;
    }

    public T getRoot() {
        return root.element;
    }

    public BinaryTree<T> getLeft() {
        BinaryTree<T> t = new BinaryTree<>();
        t.root = root.left;
        return t;
    }

    public BinaryTree<T> getRight() {
        BinaryTree<T> t = new BinaryTree<>();
        t.root = root.right;
        return t;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BinaryTree<?> that = (BinaryTree<?>) o;

        return root != null ? root.equals(that.root) : that.root == null;
    }
    
}
