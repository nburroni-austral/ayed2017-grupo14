package main.binaryTree;

import java.io.Serializable;

/**
 * Created by Brian Froschauer on 4/4/2017.
 */

public class DoubleNode <T> implements Serializable{
    public DoubleNode <T> right;
    public DoubleNode <T> left;
    public T element;

    public DoubleNode(T element) {
        this.element = element;
    }

    public DoubleNode(T element, DoubleNode<T> left, DoubleNode<T> right) {
        this.element = element;
        this.right = right;
        this.left = left;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DoubleNode<?> that = (DoubleNode<?>) o;

        if (right != null ? !right.equals(that.right) : that.right != null) return false;
        if (left != null ? !left.equals(that.left) : that.left != null) return false;
        return element != null ? element.equals(that.element) : that.element == null;
    }

}
