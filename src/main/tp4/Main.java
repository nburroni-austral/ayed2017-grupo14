package main.tp4;

/**
 * Created by matiasvenditti on 4/5/17.
 */
public class Main {
    public static void main(String[] args) {

        PriorityQueue<String> priorityQueue = new PriorityQueue<>(3, 4);

        priorityQueue.enqueue("Priority 1 (first)", 1);
        priorityQueue.enqueue("Priority 1 (second)", 1);
        priorityQueue.enqueue("Priority 1 (third)", 1);
        priorityQueue.enqueue("Priority 1 (forth)", 1);

        priorityQueue.enqueue("Priority 2 (first)", 2);
        priorityQueue.enqueue("Priority 2 (second)", 2);
        priorityQueue.enqueue("Priority 2 (third)", 2);
        priorityQueue.enqueue("Priority 2 (forth)", 2);

        priorityQueue.enqueue("Priotity 3 (first)", 3);
        priorityQueue.enqueue("Priotity 3 (second)", 3);
        priorityQueue.enqueue("Priotity 3 (third)", 3);
        priorityQueue.enqueue("Priotity 3 (forth)", 3);

        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());

        priorityQueue.enqueue("Priority 1 (fifth)", 1);
        priorityQueue.enqueue("Priority 1 (sixth)", 1);
        priorityQueue.enqueue("Priority 2 (fifth)", 2);
        priorityQueue.enqueue("Priority 2 (sixth)", 2);

        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
        System.out.println(priorityQueue.dequeue());
    }
}
