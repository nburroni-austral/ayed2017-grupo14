package main.tp4;

import main.queue.DynamicQueue;
import main.stack.DynamicStack;

/**
 * Created by matiasvenditti on 4/9/17.
 */
public class Palindrome {

    public static boolean palindrome(String s){
        DynamicQueue<Character> queue = new DynamicQueue<>(s.length());
        DynamicStack<Character> stack = new DynamicStack<>();

        for(int i = 0; i< s.length(); i++){
            queue.enqueue(s.charAt(i));
            stack.push(s.charAt(i));
        }
        for(int i = 0; i < s.length(); i++){
            Character queueChar = queue.dequeue();
            Character stackChar = stack.peek();
            if (queueChar != stackChar){
                return false;
            }
            else{
                stack.pop();
            }

        }
        return true;
    }

}
