package main.tp4.Bank;

/**
 * Created by matiasvenditti on 4/10/17.
 */
public class Simulation {

    public static void main(String[] args) {
        double timeSimulated = 18000;

        Bank bank1 = new Bank(new RTCBankA());
//        Bank bank2 = new Bank(new RTCBankB());
        double counter = 0;
        while(counter < timeSimulated){
            bank1.getStrategy().update(counter);
//            bank2.getStrategy().update();
            counter++;
        }
        System.out.println(bank1.getStrategy().getStatistics());
        System.out.println("------------------------");
//        System.out.println(bank2.getStrategy().getStatistics());
        System.out.println("Total time: " + timeSimulated/3600 + " hours.");
    }


}
