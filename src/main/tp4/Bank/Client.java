package main.tp4.Bank;

/**
 * Created by matiasvenditti on 4/10/17.
 */
public class Client {

    private int number;

    public Client(int number){
        this.number = number;
    }

    public boolean decide(){
        double random = Math.random();
        if (number <= 3){
            return true;
        }
        else if (number <= 8 && number > 4){
            return (random <= 0.25)? true : false;
        }
        else{
            return (random <= 0.5)? true : false;
        }
    }
}
