package main.tp4.Bank;

/**
 * Created by matiasvenditti on 4/12/17.
 */
public abstract class Strategy {
    protected int servedClients;
    protected int totalClients;
    protected int reusedClients;
    protected double servingTime;
    protected ATM[] atms = new ATM[3];
    protected double godTime;

    public Strategy(){
        this.servedClients = 0;
        this.totalClients = 0;
        this.reusedClients = 0;
        this.servingTime = 0;
        this.godTime = 0;
        atms[0] = new ATM(0.5,1.5, 1);
        atms[1] = new ATM(0.5, 2, 2);
        atms[2] = new ATM(0.5, 2.5, 3);
    }

    public abstract void addClient();

    public abstract void checkAvailable();

    public abstract ATM pick();

    public abstract void update(double godTime);

    public String getStatistics(){
        String result = "Served clients: " + servedClients;
        result += "\nTotal clients: " + totalClients;
        result += "\nReused clients: " + reusedClients;
        result += "\nServing time: " + servingTime/3600 + " hours.";
        return result;

    }

}
