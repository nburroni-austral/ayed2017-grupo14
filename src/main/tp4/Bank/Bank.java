package main.tp4.Bank;

/**
 * Created by matiasvenditti on 4/12/17.
 */
public class Bank {

    private Strategy strategy;

    public Bank(Strategy strategy){
        this.strategy = strategy;
    }

    public Strategy getStrategy() {
        return strategy;
    }
}
