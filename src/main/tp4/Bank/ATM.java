package main.tp4.Bank;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by matiasvenditti on 4/10/17.
 */
public class ATM {

    private double lowTime;
    private double highTime;
    private boolean isServing;
    private boolean isDone;
    private double timeOfLastService;
    private double serviceTime;
    private int id;
    private Client currentClient;

    public ATM(double lowTime, double highTime, int id){
        this.lowTime = lowTime;
        this.highTime = highTime;
        this.timeOfLastService = 0;
        this.serviceTime = 0;
        this.id = id;
    }

    /**
     * Generates the time of service required for the ATM.
     */
    public void generateTime(){
        serviceTime =  (Math.random() * (highTime - lowTime) + lowTime);
    }

    /**
     * Indicates if the ATM is currently serving.
     * @return true if its attending false if not.
     */
    public boolean isServing(){
        return isServing;
    }

    public void addCustomer(Client c ,double godTime){
        if (!isServing()){
            currentClient = c;
            generateTime();
            isServing = true;
            isDone = false;
        }
        else if (godTime - timeOfLastService > serviceTime){
            currentClient = null;
            isServing = false;
            isDone = true;
            timeOfLastService = System.currentTimeMillis()/1000;
        }
    }

    /**
     * Indicates if the ATM finished attending.
     * @return true if it is done, false if not.
     */
    public boolean isDone() {
        return isDone;
    }

    /**
     * Provides the service time of the ATM
     * @return service time.
     */
    public double getServiceTime() {
        return serviceTime;
    }


    public void setCurrentClient(Client currentClient) {
        this.currentClient = currentClient;
    }
}
