package main.tp4.Bank;

import main.queue.DynamicQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by matiasvenditti on 4/10/17.
 */
public class RTCBankA extends Strategy{

    private ATM[] atms = new ATM[3];
    private DynamicQueue<Client> clients;
    private List<ATM> possibleATMs;

    public RTCBankA(){
        super();
        atms[0] = new ATM(0.5,1.5, 1);
        atms[1] = new ATM(0.5, 2, 2);
        atms[2] = new ATM(0.5, 2.5, 3);
        clients = new DynamicQueue<>(75);
        possibleATMs = new ArrayList<>();
    }

    /**
     * Adds client to the queue if he decides to stay.
     */
    public void addClient(){
        Random random = new Random();
        int size = random.nextInt(5);
        Client[] clientArray = new Client[size];
        totalClients += size;
        for (int i = 0; i< clientArray.length; i++){
            clientArray[i] = new Client(clients.length());
        }
        for (int i = 0; i < clientArray.length; i++){
            if (clientArray[i].decide()){
                clients.enqueue(clientArray[i]);
            }
            else{
                reusedClients+= size;
            }
        }
    }

    /**
     * Adds every available ATM to the possible ATMs list.
     */
    public void checkAvailable(){
        ArrayList<ATM> list = new ArrayList<>();
        for (int i = 0; i < atms.length; i++){
            if (!atms[i].isServing()){
                list.add(atms[i]);
            }
        }
        possibleATMs = list;
    }


    /**
     * Picks from every ATM in the possible ATMs
     * @return an available ATM.
     */
    public ATM pick(){
        return atms[(int)(Math.random() * possibleATMs.size())];
    }


    public void update(double godTime){
//        godTime = System.currentTimeMillis()/(60*1000*60);
        addClient();
        checkAvailable();
        int iterations = (clients.length() <= possibleATMs.size())? clients.length() : possibleATMs.size();
        for (int i = 0; i < iterations; i++){
            pick().addCustomer(clients.dequeue(), godTime);
        }
        for (ATM atm: atms) {
            if (atm.isDone()) {
                servedClients++;
                servingTime += atm.getServiceTime(); //EN SEGUNDOS.
                atm.setCurrentClient(null);
            }
        }

    }

}
