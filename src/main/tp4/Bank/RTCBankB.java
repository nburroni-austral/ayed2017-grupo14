package main.tp4.Bank;

import main.queue.DynamicQueue;

import java.util.ArrayList;

/**
 * Created by matiasvenditti on 4/10/17.
 */
public class RTCBankB extends Strategy{

    private DynamicQueue<Client>[] clients = new DynamicQueue[3];
    private DynamicQueue<Client> auxiliaryQueue = new DynamicQueue<>(75);
    private ArrayList<ATM> possibleATMs;
    private ArrayList<DynamicQueue<Client>> possibleQueues;

    public RTCBankB(){
        super();
        possibleATMs = new ArrayList<>();
        possibleQueues = new ArrayList<>();
        for (int i = 0; i < clients.length; i++){
            clients[i] = new DynamicQueue<>(25);
        }
    }

    public void addClient(){
        totalClients++;
        Client client = new Client(auxiliaryQueue.length());
        if (client.decide()){
            auxiliaryQueue.enqueue(client);
        }
        else{
            reusedClients++;
        }
        addClientToQueue(auxiliaryQueue.dequeue());
    }

    private void addClientToQueue(Client client){
        ArrayList<DynamicQueue<Client>> result = new ArrayList<>();
        for (int i = 0; i< clients.length; i++){
            if (clients[i].length() == 0){
                result.add(clients[i]);
            }
        }
        possibleQueues = result;
        if (possibleQueues.size() == 0){
            int min = clients[0].length();
            DynamicQueue minimum = clients[0];
            for (int i = 1; i < clients.length; i++){
                if (min > clients[i].length()){
                    min = clients[i].length();
                    minimum = clients[i];
                }
                else if (min == clients[i].length()){
                    possibleQueues.add(minimum);
                    minimum = clients[i];
                    if (i == clients.length -1){
                        possibleQueues.add(clients[i]);
                    }
                }
            }
            possibleQueues.add(minimum);
        }
        else{
            pickQueue().enqueue(client);
        }
        pickQueue().enqueue(client);
    }

    public void checkAvailable(){
        ArrayList<ATM> list = new ArrayList<>();
        for (int i = 0; i < atms.length; i++){
            if (!atms[i].isServing()){
                list.add(atms[i]);
            }
        }
        possibleATMs = list;
    }

    public ATM pick(){
        return atms[(int)(Math.random() * possibleATMs.size())];
    }

    public DynamicQueue pickQueue(){
        return clients[(int)(Math.random()* possibleQueues.size())];
    }

    public void update(double godTime){
        addClient();
//        checkAvailable();
//        serve();
        for (ATM atm: atms) {
            if (atm.isDone()) {
                servedClients++;
                servingTime += atm.getServiceTime(); //EN SEGUNDOS.
                atm.setCurrentClient(null);
            }
        }

    }

}
