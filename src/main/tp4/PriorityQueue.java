package main.tp4;

import main.queue.DynamicQueue;

import java.util.ArrayList;

/**
 * Created by matiasvenditti on 4/9/17.
 */
public class PriorityQueue<T> {

    private DynamicQueue<T>[] queueArray;

    public PriorityQueue(int amountOfPriorities, int size){
        queueArray = new DynamicQueue[amountOfPriorities];
        for (int i = 0; i < amountOfPriorities; i++){
            queueArray[i] = new DynamicQueue<T>(size);
        }
    }

    public void enqueue(T data, int priority){
        for (int i = 0; i < queueArray.length; i++){
            if (i+1 == priority){
                queueArray[i].enqueue(data);
            }
        }
    }

    public T dequeue(){
        for (int i = 0; i < queueArray.length; i++){
            if (!queueArray[i].isEmpty()){
                return queueArray[i].dequeue();
            }
        }
        throw new RuntimeException("Every queue is empty.");
    }


}
