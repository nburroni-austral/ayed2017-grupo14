package main.sudoku;

import javax.swing.*;
import java.awt.*;

/**
 * Created by matiasvenditti on 3/30/17.
 */
public class SudokuFrame extends JFrame {

    private int[][] values;
    private SudokuButton[][] buttons;
    private JButton[] interactiveButtons = {new JButton("Solve"), new JButton("Load first example"), new JButton("Load second example"), new JButton("Empty")};

    /**
     * Constructor method creates a Sudoku window.
     */
    public SudokuFrame(){
        super("Sudoku Solver");
        this.values = new int[9][9];
        this.buttons = new SudokuButton[9][9];
        setFirstLayout();
        setGrids(values);
        centerFrame();
        init();
    }

    /**
     * Positions the frame in the middle of the screen.
     */
    private void centerFrame(){
        setSize(800,640);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2 - getSize().width/2,dim.height/2 - getSize().height/2);
    }

    /**
     * Makes the window visible and makes it possible for it to be quit by exiting.
     */
    private void init(){
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    /**
     *
     * @param values
     */
    private void setGrids(int[][] values){
        JPanel gridPanel = new JPanel();
        gridPanel.setLayout(new GridLayout(9,9));
        for (int i = 0; i < buttons.length; i++) {
            for (int j = 0; j < buttons[i].length; j++) {
                buttons[i][j] = new SudokuButton("");
                values[i][j] = 0;
                gridPanel.add(buttons[i][j]);
            }
        }
        add(gridPanel, BorderLayout.CENTER);
    }

    /**
     * Sets the bottom panel for the interactive buttons such as, solve, load example, or empty.
     */
    private void setFirstLayout(){
        getContentPane().setLayout(new BorderLayout());
        JPanel solvePanel = new JPanel();
        solvePanel.setLayout(new FlowLayout());
        for (int i = 0; i < interactiveButtons.length; i++) {
            solvePanel.add(interactiveButtons[i]);
        }
        add(solvePanel, BorderLayout.SOUTH);

    }

    public int[][] getValues() {
        return values;
    }

    public SudokuButton[][] getButtons() {
        return buttons;
    }

    public JButton getSolve() {
        return interactiveButtons[0];
    }

    public JButton getLoad1() {
        return interactiveButtons[1];
    }

    public JButton getLoad2(){ return interactiveButtons[2]; }

    public JButton getEmpty() {
        return interactiveButtons[3];
    }


    /**
     * Method setting the values at the values matrix at a specific location.
     * @param i coordinate.
     * @param j coordinate.
     * @param value to be set.
     */
    public void setValues(int i, int j, int value){
        for (int k = 0; k < buttons.length; k++) {
            for (int l = 0; l < buttons[k].length; l++) {
                if (k == i && l == j){
                    values[k][l] = value;
                }
            }
        }
    }


    /**
     * Goes through the values array, setting the buttons message to the value at the
     * position i, j of the values matrix.
     * @param values matrix containing values to be passed on the buttons.
     */
    public void showValues(int[][] values){
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[i].length; j++) {
                if (values[i][j] == 0){
                    buttons[i][j].setText("");
                }
                else{
                    buttons[i][j].setText(values[i][j] + "");
                }
            }
        }
    }

}
