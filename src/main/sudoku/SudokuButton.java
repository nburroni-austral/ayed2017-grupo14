package main.sudoku;

import javax.swing.*;

/**
 * Created by matiasvenditti on 4/4/17.
 */
public class SudokuButton extends JButton {

    int counter;

    public SudokuButton(String text){
        super(text);
        counter = 0;
    }

    public void incrementCounter(){
        counter++;
    }

    public int getCounter(){
        return counter;
    }

    public void setCounter(int value){
        counter = value;
    }
}
