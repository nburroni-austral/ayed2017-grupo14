package main.sudoku;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by matiasvenditti on 4/3/17.
 */
public class SudokuController {

    private SudokuFrame views;

    public SudokuController(){
        views = new SudokuFrame();
        setButtonListeners();
    }

    /**
     * Sets the solve, load example 1, load example 2, and empty buttons its corresponding
     * listeners, plus adding the grid buttons its listeners.
     */
    private void setButtonListeners(){

        int grid1[][] =
                {{3,0,6,5,0,8,4,0,0},
                {5,2,0,0,0,0,0,0,0},
                {0,8,7,0,0,0,0,3,1},
                {0,0,3,0,1,0,0,8,0},
                {9,0,0,8,6,3,0,0,5},
                {0,5,0,0,9,0,6,0,0},
                {1,3,0,0,0,0,2,5,0},
                {0,0,0,0,0,0,0,7,4},
                {0,0,5,2,0,6,3,0,0}};

        int empty[][] =
                {{0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0}};

        int grid2[][] =
                {{7,6,0,0,0,9,0,1,0},
                {8,0,0,0,0,0,7,2,4},
                {0,0,0,7,1,4,0,0,9},
                {4,0,6,0,0,0,3,0,2},
                {0,7,0,4,5,6,0,0,0},
                {0,5,1,0,0,0,0,7,6},
                {1,0,0,2,9,0,0,4,0},
                {2,0,7,6,3,0,1,0,0},
                {0,9,8,0,0,5,2,0,7}};

        views.getLoad1().addActionListener(new LoadExampleListener(grid1, views));
        views.getLoad2().addActionListener(new LoadExampleListener(grid2,views));
        views.getEmpty().addActionListener(new EmptyListener(views));
        views.getSolve().addActionListener(new SolveListener(views));
        for(int i = 0; i < views.getButtons().length; i++){
            for (int j = 0; j < views.getButtons()[i].length; j++) {
                views.getButtons()[i][j].addActionListener(new ButtonListener(i,j, views));
            }
        }
    }


    /**
     * Listener for the grid buttons.
     */
    private class ButtonListener implements ActionListener{
        private int i,j;
        private SudokuFrame views;

        /**
         * Constructor method with i, j coordinates to make it possible to
         * add button listeners on the grid at a specific location.
         * @param i position.
         * @param j position.
         * @param views instance of the main.sudoku frame.
         */
        public ButtonListener(int i, int j, SudokuFrame views){
            this.i = i;
            this.j = j;
            this.views = views;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int counter = views.getButtons()[i][j].getCounter();
            if (counter < 9){
                views.setValues(i, j,counter+1);
                views.getButtons()[i][j].incrementCounter();
                views.showValues(views.getValues());
            }
            else{
                views.setValues(i, j,0);
                views.getButtons()[i][j].setCounter(0);
                views.showValues(views.getValues());
            }
        }
    }

    /**
     * Listener for the solve button.
     */
    private class SolveListener implements ActionListener{
        private SudokuFrame views;
        private SudokuModel model;

        /**
         * Constructor method receiving instance of the main.sudoku views.
         * @param views
         */
        public SolveListener(SudokuFrame views){
            this.views = views;
            this.model = new SudokuModel(views.getValues());
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            SudokuModel.solve();
            views.showValues(model.getGrid());

        }
    }

    /**
     * Listener for the Load buttons.
     */
    private class LoadExampleListener implements ActionListener{
        private SudokuFrame views;
        private int[][] fixedMatrix;

        /**
         * Constructor method requires int[][] being the example to load, and the views to set its values.
         * @param fixedMatrix containing the values to pass.
         * @param views instance of the views to which values will be set.
         */
        public LoadExampleListener(int[][] fixedMatrix, SudokuFrame views){
            this.fixedMatrix = fixedMatrix;
            this.views = views;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < fixedMatrix.length; i++) {
                for (int j = 0; j < fixedMatrix[i].length; j++) {
                    views.setValues(i,j,fixedMatrix[i][j]);
                    views.showValues(fixedMatrix);
                }
            }

        }
    }

    /**
     * Listener for the empty button, empties every button in the grid.
     */
    private class EmptyListener implements ActionListener{
        private SudokuFrame views;
        private int[][] grid;

        /**
         * Constructor method
         * @param views instance of the main.sudoku frame.
         */
        public EmptyListener(SudokuFrame views){
            this.views = views;
            grid = new int[views.getValues().length][views.getValues().length];
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    grid[i][j] = 0;
                }
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    views.getButtons()[i][j].setCounter(0);
                    views.setValues(i,j,0);
                    views.showValues(grid);
                }
            }
        }
    }


}
