package main.sudoku;

/**
 * Created by matiasvenditti on 4/4/17.
 */
public class SudokuModel {

    private static int[][] grid;

    /**
     * Constructor method, creates a main.sudoku model.
     * @param grid representing the matrix to solve.
     */
    public SudokuModel(int[][] grid){
        this.grid = grid;
    }

    public static boolean solve(){
        int row;
        int col;
        int[] emptySpaces = findEmpty();
        row = emptySpaces[0];
        col = emptySpaces[1];
        if (row == -1) return true;
        for (int i = 1; i <= 9; i++) {
            if (!checkRowAndColumn(row, col, i) && !checkCell(row / 3 * 3, col / 3 * 3, i)){
                grid[row][col] = i;
                if (solve()){
                    return true;
                }
                grid[row][col] = 0;
            }
        }
        return false;
    }

    /**
     *
     * @return grid matrix.
     */
    public static int[][] getGrid() {
        return grid;
    }

    /**
     * Finds first empty space in the matrix, and saves its coordinates in an array of size 2.
     * @return array containing the coordinates of the first empty space available.
     */
    private static int[] findEmpty(){
        int[] emptyCoordinates = new int[2];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 0){
                    emptyCoordinates[0] = i;
                    emptyCoordinates[1] = j;
                    return emptyCoordinates;
                }
            }
        }
        emptyCoordinates[0] = -1;
        emptyCoordinates[1] = -1;
        return emptyCoordinates;
    }

    /**
     * Method returning a boolean indicating wether a value is found in a specified row or column.
     * @param row row to check.
     * @param col column to check.
     * @param n value to check.
     * @return true if the element is found in the specified row or column.
     */
    private static boolean checkRowAndColumn(int row, int col, int n) {
        for (int i = 0; i < 9; i++) {
            if (grid[row][i] == n || grid[i][col] == n) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method returning a boolean indicating wether a value is found in a specified sector or group of cells.
     * @param squareStartRow
     * @param squareStartCol
     * @param n
     * @return
     */
    private static boolean checkCell(int squareStartRow, int squareStartCol, int n) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (grid[i + squareStartRow][j + squareStartCol] == n) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Converts the grid to String.
     * @return String equivalent of the matrix.
     */
    public String toString(){
        String result = "{";
        for (int i = 0; i < grid.length; i++) {
            result += "[";
            for (int j = 0; j < grid[i].length; j++) {
                result += grid[i][j] + " ";
            }
            result += "]\n";
        }
        result += "}";
        return result;
    }
}
