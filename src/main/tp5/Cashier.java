package main.tp5;

import main.queue.DynamicQueue;

/**
 * Created by matiasvenditti on 4/19/17.
 */
public class Cashier {

    private DynamicQueue<Passenger> passengers;
    private static final double PRICE = 0.7;
    private double profit;
    private int idleTime;

    public Cashier(){
        passengers = new DynamicQueue<>(500000);
        profit = 0;
        idleTime = 0;
    }

    public int attend(int leavingTime){
        double random = Math.random();
        if (random <= 0.3){
            Passenger passenger = passengers.dequeue();
            profit += PRICE;
            return passenger.getWaitingTime(leavingTime);
        }
        else{
            return 0;
        }
    }

    public int finalAttend(){
        int attendedPassengers = 0;
        while(!passengers.isEmpty()){
            profit+= PRICE;
            passengers.dequeue();
            attendedPassengers++;
        }
        return attendedPassengers;
    }

    public DynamicQueue<Passenger> getPassengers() {
        return passengers;
    }

    public double getProfit() {
        return profit;
    }

    public int getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(int idleTime) {
        this.idleTime += idleTime;
    }
}
