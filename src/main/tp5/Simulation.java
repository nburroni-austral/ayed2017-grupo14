package main.tp5;

import java.util.Scanner;

/**
 * Created by matiasvenditti on 4/19/17.
 */
public class Simulation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert amount of cashiers: ");
        int amount = scanner.nextInt();
        TrainSystem trainSystem = new TrainSystem(amount);
        int time = 0;
        while(time < 57570){
            trainSystem.distributePassengers(generatePassengers(time));
            trainSystem.attend(time);
            time += 10;
        }
        while(time < 57600){
            trainSystem.distributePassengers(generatePassengers(time));
            trainSystem.finalAttend();
            time += 10;
        }
        System.out.println(trainSystem.getStatistics());
    }

    public static Passenger[] generatePassengers(int timeOfArrival){
        Passenger[] passengersGenerated = new Passenger[5];
        for(int i = 0; i < passengersGenerated.length; i++){
            passengersGenerated[i] = new Passenger(timeOfArrival);
        }
        return passengersGenerated;
    }
}
