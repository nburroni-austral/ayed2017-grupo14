package main.tp5;

/**
 * Created by matiasvenditti on 4/19/17.
 */
public class Passenger {

    private int arrivalTime;

    public Passenger(int arrivalTime){
        this.arrivalTime = arrivalTime;
    }

    public int getWaitingTime(int leavingTime){
        return leavingTime - arrivalTime;
    }
}
